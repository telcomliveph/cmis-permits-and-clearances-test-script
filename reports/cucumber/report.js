$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("PermitsAndClearances/NonCommercialPermit.feature");
formatter.feature({
  "line": 2,
  "name": "Apply permit for Non-Commercial Video and Photography",
  "description": "",
  "id": "apply-permit-for-non-commercial-video-and-photography",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@NONCOMMERCIALPERMIT"
    }
  ]
});
formatter.scenario({
  "comments": [
    {
      "line": 4,
      "value": "#@NONCOMMERCIALPERMIT-001"
    },
    {
      "line": 5,
      "value": "#Scenario: Apply permit for non-commercial video and photography w/ blank fields"
    },
    {
      "line": 6,
      "value": "#\tGiven That user will go to the customer portal"
    },
    {
      "line": 7,
      "value": "#\tAnd then click the Apply BMD Services menu"
    },
    {
      "line": 8,
      "value": "#\tAnd click Apply Application button for permit for non-commercial video and photography"
    },
    {
      "line": 9,
      "value": "#\tAnd the user will leave all the fields for permit for non-commercial video and photography blank"
    },
    {
      "line": 10,
      "value": "#\tWhen the blank form for permit for non-commercial video and photography is submitted"
    },
    {
      "line": 11,
      "value": "#\tThen required validation messages will appear"
    },
    {
      "line": 12,
      "value": "#"
    }
  ],
  "line": 13,
  "name": "Apply permit for non-commercial video and photography while leaving the middle name and suffix fields empty",
  "description": "",
  "id": "apply-permit-for-non-commercial-video-and-photography;apply-permit-for-non-commercial-video-and-photography-while-leaving-the-middle-name-and-suffix-fields-empty",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 14,
  "name": "That user will go to the customer portal",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "then click the Apply BMD Services menu",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "click Apply Application button for permit for non-commercial video and photography",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "the user will fill up the form for permit for non-commercial video and photography leaving the middle name and suffix fields empty",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "the user will submit the form permit for non-commercial video and photography",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "the user will go to the dashboard page to check submitted UPCDD application",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "click the Permits an Clearances menu to check submitted UPCDD application",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "then click the Pending Applications menu to check submitted UPCDD application",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "the user goes to the last part of the table to look for the to check submitted UPCDD application",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "the submitted UPCDD application should be present",
  "keyword": "Then "
});
formatter.match({
  "location": "ApplyBMDServices.that_user_will_go_to_the_customer_portal()"
});
formatter.result({
  "duration": 10257513900,
  "status": "passed"
});
formatter.match({
  "location": "ApplyBMDServices.then_click_the_Apply_BMD_Services_menu()"
});
formatter.result({
  "duration": 1265677300,
  "status": "passed"
});
formatter.match({
  "location": "ApplyBMDServices.click_Apply_Application_button_for_permit_for_non_commercial_video_and_photography()"
});
formatter.result({
  "duration": 2336601500,
  "status": "passed"
});
formatter.match({
  "location": "ApplyBMDServices.the_user_will_fill_up_the_form_for_permit_for_non_commercial_video_and_photography_leaving_the_middle_name_and_suffix_fields_empty()"
});
formatter.result({
  "duration": 13514699500,
  "status": "passed"
});
formatter.match({
  "location": "ApplyBMDServices.the_user_will_submit_the_form_permit_for_non_commercial_video_and_photography()"
});
