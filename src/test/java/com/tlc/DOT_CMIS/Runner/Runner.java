package com.tlc.DOT_CMIS.Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions (
	features = {"src/test/java/com/tlc/DOT_CMIS/FeatureFiles/"},
	glue = {"com.tlc.DOT_CMIS.StepFiles"},
	plugin = {"pretty", "html:reports/cucumber", 
			"json:reports/DOT_CMIS_REPORT.json", 
			"com.cucumber.listener.ExtentCucumberFormatter:reports/DOT_CMIS_REPORT.html"},
	monochrome = true,
	tags = {"@NONCOMMERCIALPERMIT"}
)

public class Runner {

}
