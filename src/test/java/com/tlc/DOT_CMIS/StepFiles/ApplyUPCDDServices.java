package com.tlc.DOT_CMIS.StepFiles;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.selenium.helper.Elements;
import com.selenium.helper.Util;
import com.tlc.DOT_CMIS.SharedClass;
import com.tlc.DOT_CMIS.Helpers.Byloc;
import com.tlc.DOT_CMIS.Helpers.Reuse;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ApplyUPCDDServices {

	WebDriver driver;
	Elements el;
	static Reuse re;
	
	String email = "";
	String url2 = "http://119.8.63.161:84/dashboard";

    String no1 = "545",
    	   no2 = "77";
    
    String no3 = "96325",
     	   no4 = "8089";

	public ApplyUPCDDServices(SharedClass sharedClass) {
		driver = sharedClass.setup();
		el = new Elements(driver, 3);
		re = new Reuse(driver);
	}
	
	@And("^then click the Apply UPCDD Services menu$")
	public void then_click_the_Apply_UPCDD_Services_menu() throws Throwable {
	    re.click(Byloc.CustomerPortal.applyUPCDDLnk, "Apply UPCDD Services");
	}

	@And("^click Apply Application button for locational clearance$")
	public void click_Apply_Application_button_for_locational_clearance() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.applyLocationalBtn);
	    re.scrollTo(Byloc.CustomerPortal.applyLocationalBtn, driver);
	    re.click(Byloc.CustomerPortal.applyLocationalBtn, "Apply Application");
	}

	@And("^then fill up the form for locational clearance leaving some fields blank$")
	public void the_fill_up_the_form_for_locational_clearance_leaving_some_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.locationalLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.locationalLastNameFld , "Last name", "Guevarra");
	    re.getTextbox(Byloc.CustomerPortal.locationalFirstNameFld , "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.locationalMiddleNameFld , "Middle name", "Mendrano");
	    re.getTextbox(Byloc.CustomerPortal.locationalSuffixFld , "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.locationalAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalTelNoFld, "Telephone no.", "5456663");
	    re.getTextbox(Byloc.CustomerPortal.locationalMobileNoFld, "Mobile no.", "0988888"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.locationalBusinessNameFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessLocationFld, "Business name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessLocationFld, "Business location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalLineOfBusinessFld, "Line of Business", "Mobile Money");
	}

	@When("^the locational clearance form is submitted$")
	public void the_locational_clearance_form_is_submitted() throws Throwable {
	    re.click(Byloc.CustomerPortal.submitLocationalBtn, "Submit");
	    re.click(Byloc.confirmSubmit, "Submit");
	}

	@Then("^the submission of locational clearance form will not proceed$")
	public void the_submission_of_locational_clearance_form_will_not_proceed() throws Throwable {
		Thread.sleep(3000);
	    boolean con = driver.findElements(Byloc.invalidRequest).isEmpty();
	    Assert.assertEquals(false,con);
	}
	
	@And("^then fill up the form for locational clearance w/ invalid email format$")
	public void the_fill_up_the_form_for_locational_clearance_w_invalid_email_format() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.locationalLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.locationalLastNameFld , "Last name", "Guevarra");
	    re.getTextbox(Byloc.CustomerPortal.locationalFirstNameFld , "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.locationalMiddleNameFld , "Middle name", "Mendrano");
	    re.getTextbox(Byloc.CustomerPortal.locationalSuffixFld , "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.locationalAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalTelNoFld, "Telephone no.", "5456663");
	    re.getTextbox(Byloc.CustomerPortal.locationalMobileNoFld, "Mobile no.", "0988888"+Util.randomNum(9999, 1111));
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"email.com";
	    re.getTextbox(Byloc.CustomerPortal.locationalEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.locationalBusinessNameFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessLocationFld, "Business name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessLocationFld, "Business location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalLineOfBusinessFld, "Line of Business", "Mobile Money");
	}
	
	@And("^then fill up the form for locational clearance w/ invalid telephone no\\.$")
	public void the_fill_up_the_form_for_locational_clearance_w_invalid_telephone_no() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.locationalLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.locationalLastNameFld , "Last name", "Guevarra");
	    re.getTextbox(Byloc.CustomerPortal.locationalFirstNameFld , "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.locationalMiddleNameFld , "Middle name", "Mendrano");
	    re.getTextbox(Byloc.CustomerPortal.locationalSuffixFld , "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.locationalAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalTelNoFld, "Telephone no.", "54456354634556663");
	    re.getTextbox(Byloc.CustomerPortal.locationalMobileNoFld, "Mobile no.", "0988888"+Util.randomNum(9999, 1111));
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.locationalEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.locationalBusinessNameFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessLocationFld, "Business name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessLocationFld, "Business location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalLineOfBusinessFld, "Line of Business", "Mobile Money");
	}
	
	@And("^then fill up the form for locational clearance w/ invalid mobile no\\.$")
	public void the_fill_up_the_form_for_locational_clearance_w_invalid_mobile_no() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.locationalLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.locationalLastNameFld , "Last name", "Guevarra");
	    re.getTextbox(Byloc.CustomerPortal.locationalFirstNameFld , "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.locationalMiddleNameFld , "Middle name", "Mendrano");
	    re.getTextbox(Byloc.CustomerPortal.locationalSuffixFld , "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.locationalAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalTelNoFld, "Telephone no.", "5456788");
	    re.getTextbox(Byloc.CustomerPortal.locationalMobileNoFld, "Mobile no.", "098845645645888"+Util.randomNum(9999, 1111));
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.locationalEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.locationalBusinessNameFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessLocationFld, "Business name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessLocationFld, "Business location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalLineOfBusinessFld, "Line of Business", "Mobile Money");
	}
	
	@And("^then fill up the form for locational clearance w/ valid and complete details$")
	public void the_fill_up_the_form_for_locational_clearance_w_valid_and_complete_details() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.locationalLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.locationalLastNameFld , "Last name", "Guevarra");
	    re.getTextbox(Byloc.CustomerPortal.locationalFirstNameFld , "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.locationalMiddleNameFld , "Middle name", "Mendrano");
	    re.getTextbox(Byloc.CustomerPortal.locationalSuffixFld , "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.locationalAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalTelNoFld, "Telephone no.", "5456788");
	    re.getTextbox(Byloc.CustomerPortal.locationalMobileNoFld, "Mobile no.", "0988456"+Util.randomNum(9999, 1111));
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.locationalEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.locationalBusinessNameFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessNameFld, "Business name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessLocationFld, "Business location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalLineOfBusinessFld, "Line of Business", "Mobile Money");
	}

	@And("^the user submit the Locational Clearance form$")
	public void the_user_submit_the_Locational_Clearance_form() throws Throwable {
		the_locational_clearance_form_is_submitted();
	}
	
	@When("^the submit button for locational permit is clicked$")
	public void the_submit_button_for_locational_permit_is_clicked() throws Throwable {
		re.click(Byloc.CustomerPortal.submitLocationalBtn, "Submit");
	}
	
	@And("^click Apply Application button for excavation clearance$")
	public void click_Apply_Application_button_for_excavation_clearance() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.applyExcavationBtn);
	    re.click(Byloc.CustomerPortal.applyExcavationBtn, "Apply Application");
	}

	@And("^then fill up the form for excavation clearance leaving some fields blank$")
	public void the_fill_up_the_form_for_excavation_clearance_leaving_some_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.excavationCompanyNameFld);
	    re.getTextbox(Byloc.CustomerPortal.excavationCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.excavationCompanyAddressFld, "Company Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationMobileNoFld, "Mobile Number", "09888889999");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.excavationPurposeFld, "Purpose", "Purpose");
	    re.scrollTo(Byloc.CustomerPortal.excavationLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.excavationRepresentativeFld, "Authorized Representative", "John Doe");
	    re.getTextbox(Byloc.CustomerPortal.excavationLastNameFld, "Lasst Name", "Villareal");	
	    re.getTextbox(Byloc.CustomerPortal.excavationFirstNameFld, "First Name", "Angelo");
	    re.getTextbox(Byloc.CustomerPortal.excavationSuffixFld, "Suffix", "N/A");
	}
	
	@Then("^the submitted application for locational clearance should be present$")
	public void the_submitted_application_for_locational_clearance_should_be_present() throws Throwable {
		Thread.sleep(2000);
	    Util.print(email);
	    re.scrollTo(Byloc.BMDPortal.footer, driver);
	    boolean con = driver.findElements(By.xpath("//*[contains(text(), '"+email+"')]")).isEmpty();
	    Assert.assertEquals(false, con);
	}
	
	@And("^the user will go to the dashboard page to check submitted UPCDD application$")
	public void the_user_will_go_to_the_dashboard_page_to_check_submitted_UPCDD_application() throws Throwable {
		Thread.sleep(5000);
		this.driver.navigate().to(url2);
	}

	@And("^click the Permits an Clearances menu to check submitted UPCDD application$")
	public void click_the_Permits_an_Clearances_menu_to_check_submitted_UPCDD_application() throws Throwable {
		re.click(Byloc.BMDPortal.permitsAndClearancesMenu, "Permits and Clearances");
	}

	@And("^then click the Pending Applications menu to check submitted UPCDD application$")
	public void then_click_the_Pending_Applications_menu_to_check_submitted_UPCDD_application() throws Throwable {
		re.click(Byloc.BMDPortal.pendingApplicationsMenu, "Pending Applications");
	}

	@When("^the user goes to the last part of the table to look for the to check submitted UPCDD application$")
	public void the_user_goes_to_the_last_part_of_the_table_to_look_for_the_to_check_submitted_UPCDD_application() throws Throwable {
		re.click(Byloc.BMDPortal.tableLastPageBtn, "Last part of the table");
	}

	@And("^the submitted UPCDD application should be present$")
	public void the_submitted_UPCDD_application_should_be_present() throws Throwable {
		Thread.sleep(2000);
	    Util.print(email);
	    if(!driver.findElements(Byloc.BMDPortal.footer).isEmpty()) {
		    re.scrollTo(Byloc.BMDPortal.footer, driver);
	    }
	    Thread.sleep(2000);
	    boolean con = driver.findElements(By.xpath("//*[contains(text(), '"+email+"')]")).isEmpty();
	    Assert.assertEquals(false, con);
	}
	
	@When("^the excavation clearance form is submitted$")
	public void the_excavation_clearance_form_is_submitted() throws Throwable {
	    re.click(Byloc.CustomerPortal.excavationSubmit, "Submit");
	    re.click(Byloc.confirmSubmit, "Submit");
	}

	@Then("^the submission of excavation clearance form will not proceed$")
	public void the_submission_of_excavation_clearance_form_will_not_proceed() throws Throwable {
		the_submission_of_locational_clearance_form_will_not_proceed();
	}
	
	@And("^then fill up the form for excavation clearance w/ invalid email format$")
	public void the_fill_up_the_form_for_excavation_clearance_w_invalid_email_format() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.excavationCompanyNameFld);
	    re.getTextbox(Byloc.CustomerPortal.excavationCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.excavationCompanyAddressFld, "Company Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationTelNoFld, "Telephone Number", "5459655");
	    re.getTextbox(Byloc.CustomerPortal.excavationMobileNoFld, "Mobile Number", "09888889999");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"demailfdcom";
	    re.getTextbox(Byloc.CustomerPortal.excavationEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.excavationPurposeFld, "Purpose", "Purpose");
	    re.scrollTo(Byloc.CustomerPortal.excavationLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.excavationLocationFld, "Exact Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationRepresentativeFld, "Authorized Representative", "John Doe");
	    re.getTextbox(Byloc.CustomerPortal.excavationLastNameFld, "Lasst Name", "Villareal");	
	    re.getTextbox(Byloc.CustomerPortal.excavationFirstNameFld, "First Name", "Angelo");
	    re.getTextbox(Byloc.CustomerPortal.excavationMiddleNameFld, "Middle Name", "Aurelea");
	    re.getTextbox(Byloc.CustomerPortal.excavationSuffixFld, "Suffix", "N/A");
	}

	@When("^the submit button for excavation permit is clicked$")
	public void the_submit_button_for_excavation_permit_is_clicked() throws Throwable {
		re.click(Byloc.CustomerPortal.excavationSubmit, "Submit");
	}
	
	@And("^then fill up the form for excavation clearance w/ invalid telephone no\\.$")
	public void the_fill_up_the_form_for_excavation_clearance_w_invalid_telephone_no() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.excavationCompanyNameFld);
		re.getTextbox(Byloc.CustomerPortal.excavationCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.excavationCompanyAddressFld, "Company Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationTelNoFld, "Telephone Number", "54596sdf55");
	    re.getTextbox(Byloc.CustomerPortal.excavationMobileNoFld, "Mobile Number", "09888889999");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.excavationEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.excavationPurposeFld, "Purpose", "Purpose");
	    re.scrollTo(Byloc.CustomerPortal.excavationLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.excavationLocationFld, "Exact Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationRepresentativeFld, "Authorized Representative", "John Doe");
	    re.getTextbox(Byloc.CustomerPortal.excavationLastNameFld, "Lasst Name", "Villareal");	
	    re.getTextbox(Byloc.CustomerPortal.excavationFirstNameFld, "First Name", "Angelo");
	    re.getTextbox(Byloc.CustomerPortal.excavationMiddleNameFld, "Middle Name", "Aurelea");
	    re.getTextbox(Byloc.CustomerPortal.excavationSuffixFld, "Suffix", "N/A");
	}
	
	@And("^then fill up the form for excavation clearance w/ invalid mobile no\\.$")
	public void the_fill_up_the_form_for_excavation_clearance_w_invalid_mobile_no() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.excavationCompanyNameFld);
		re.getTextbox(Byloc.CustomerPortal.excavationCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.excavationCompanyAddressFld, "Company Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationTelNoFld, "Telephone Number", "5459655");
	    re.getTextbox(Byloc.CustomerPortal.excavationMobileNoFld, "Mobile Number", "09888567456745674567889999");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.excavationEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.excavationPurposeFld, "Purpose", "Purpose");
	    re.scrollTo(Byloc.CustomerPortal.excavationLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.excavationLocationFld, "Exact Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationRepresentativeFld, "Authorized Representative", "John Doe");
	    re.getTextbox(Byloc.CustomerPortal.excavationLastNameFld, "Lasst Name", "Villareal");	
	    re.getTextbox(Byloc.CustomerPortal.excavationFirstNameFld, "First Name", "Angelo");
	    re.getTextbox(Byloc.CustomerPortal.excavationMiddleNameFld, "Middle Name", "Aurelea");
	    re.getTextbox(Byloc.CustomerPortal.excavationSuffixFld, "Suffix", "N/A");
	}
	
	@And("^then fill up the form for excavation clearance w/ valid and complete details$")
	public void the_fill_up_the_form_for_excavation_clearance_w_valid_and_complete_details() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.excavationCompanyNameFld);
		re.getTextbox(Byloc.CustomerPortal.excavationCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.excavationCompanyAddressFld, "Company Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationTelNoFld, "Telephone Number", "5459655");
	    re.getTextbox(Byloc.CustomerPortal.excavationMobileNoFld, "Mobile Number", "09888675589");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.excavationEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.excavationPurposeFld, "Purpose", "Purpose");
	    re.scrollTo(Byloc.CustomerPortal.excavationLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.excavationLocationFld, "Exact Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationRepresentativeFld, "Authorized Representative", "John Doe");
	    re.getTextbox(Byloc.CustomerPortal.excavationLastNameFld, "Lasst Name", "Villareal");	
	    re.getTextbox(Byloc.CustomerPortal.excavationFirstNameFld, "First Name", "Angelo");
	    re.getTextbox(Byloc.CustomerPortal.excavationMiddleNameFld, "Middle Name", "Aurelea");
	    re.getTextbox(Byloc.CustomerPortal.excavationSuffixFld, "Suffix", "N/A");
	}

	@And("^the user submit the excavation Clearance form$")
	public void the_user_submit_the_excavation_Clearance_form() throws Throwable {
		the_excavation_clearance_form_is_submitted();
	}
	
	@And("^click Apply Application button for temporary sidewalk/street enclosure and occupancy permit$")
	public void click_Apply_Application_button_for_temporary_sidewalk_street_enclosure_and_occupancy_permit() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.nextTablePage);
	    re.click(Byloc.CustomerPortal.nextTablePage, "Next Page");
	    el.waitVisible(Byloc.CustomerPortal.nextTablePage);
	    re.click(Byloc.CustomerPortal.nextTablePage, "Next Page");
	    Thread.sleep(2000);
	    re.scrollTo(Byloc.CustomerPortal.applySidewalkEnclosureBtn, driver);
	    re.click(Byloc.CustomerPortal.applySidewalkEnclosureBtn, "Apply Application");
	}

	@And("^the user will fill up the form for enclosure permit while leaving some fields blank$")
	public void the_user_will_fill_up_the_form_for_enclosure_permit_while_leaving_some_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkCompanyNameFld);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkTelNoFld, "Telephone No.", "5456788");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkFrontageFld, "Frontage", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkSquareMeterFld, "Square Meter(s)", ""+Util.generateRandomNumber(999, 111));
	    re.scrollTo(Byloc.CustomerPortal.sidewalkLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkLocationFld, "Construction Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkRepresentativeFld, "Authorized Representative", "Sample Name");
	    
	}

	@When("^user submitted the form for enclosure permit$")
	public void user_submitted_the_form_for_enclosure_permit() throws Throwable {
	    re.click(Byloc.CustomerPortal.sidewalkSubmitBtn, "Submit");
	    re.click(Byloc.confirmSubmit, "Submit");
	}

	@Then("^the submission of enclosure permit form will not proceed$")
	public void then_the_submission_of_enclosure_permit_form_will_not_proceed() throws Throwable {
		the_submission_of_locational_clearance_form_will_not_proceed();
	}
	
	@And("^the user will fill up the form for enclosure permit w/ invalid telephone number$")
	public void the_user_will_fill_up_the_form_for_enclosure_permit_w_invalid_telephone_number() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkCompanyNameFld);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkTelNoFld, "Telephone No.", "545678dsfsd8");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkMobileNoFld, "Mobile Number", "09887767789");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkFrontageFld, "Frontage", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkWidthFld, "Width", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkSquareMeterFld, "Square Meter(s)", ""+Util.generateRandomNumber(999, 111));
	    re.scrollTo(Byloc.CustomerPortal.sidewalkLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkLocationFld, "Construction Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkPeriodFld, "Construction Period", "May 11 to Aug 11 2021");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkRepresentativeFld, "Authorized Representative", "Sample Name");
	}

	@And("^the user will fill up the form for enclosure permit w/ invalid mobile number$")
	public void the_user_will_fill_up_the_form_for_enclosure_permit_w_invalid_mobile_number() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkCompanyNameFld);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkTelNoFld, "Telephone No.", "5456788");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkMobileNoFld, "Mobile Number", "09887767dgdfgdfsdgwa789");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkFrontageFld, "Frontage", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkWidthFld, "Width", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkSquareMeterFld, "Square Meter(s)", ""+Util.generateRandomNumber(999, 111));
	    re.scrollTo(Byloc.CustomerPortal.sidewalkLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkLocationFld, "Construction Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkPeriodFld, "Construction Period", "May 11 to Aug 11 2021");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkRepresentativeFld, "Authorized Representative", "Sample Name");
	}
	
	@And("^the user will fill up the form for enclosure permit w/ valid and complete details$")
	public void the_user_will_fill_up_the_form_for_enclosure_permit_w_valid_and_complete_details() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkCompanyNameFld);
		re.getTextbox(Byloc.CustomerPortal.sidewalkCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkTelNoFld, "Telephone No.", "5456788");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkMobileNoFld, "Mobile Number", "09887767789");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkFrontageFld, "Frontage", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkWidthFld, "Width", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkSquareMeterFld, "Square Meter(s)", ""+Util.generateRandomNumber(999, 111));
	    re.scrollTo(Byloc.CustomerPortal.sidewalkLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkLocationFld, "Construction Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkPeriodFld, "Construction Period", "May 11 to Aug 11 2021");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkRepresentativeFld, "Authorized Representative", "Sample Name");
	}

	@And("^the user submit the enclosure permit form$")
	public void the_user_submit_the_enclosure_permit_form() throws Throwable {
		user_submitted_the_form_for_enclosure_permit();
	}
	
	@And("^the user will fill up the form for enclosure permit w/ invalid email$")
	public void the_user_will_fill_up_the_form_for_enclosure_permit_w_invalid_email() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkCompanyNameFld);
		re.getTextbox(Byloc.CustomerPortal.sidewalkCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkTelNoFld, "Telephone No.", "5456788");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkMobileNoFld, "Mobile Number", "09887767789");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"emailcom";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkFrontageFld, "Frontage", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkWidthFld, "Width", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkSquareMeterFld, "Square Meter(s)", ""+Util.generateRandomNumber(999, 111));
	    re.scrollTo(Byloc.CustomerPortal.sidewalkLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkLocationFld, "Construction Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkPeriodFld, "Construction Period", "May 11 to Aug 11 2021");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkRepresentativeFld, "Authorized Representative", "Sample Name");
	}

	@When("^the submit button for enclosure permit is clicked$")
	public void the_submit_button_for_enclosure_permit_is_clicked() throws Throwable {
		re.click(Byloc.CustomerPortal.sidewalkSubmitBtn, "Submit");
	}
	
	@And("^click Apply Application button for sidewalk construction permit$")
	public void click_Apply_Application_button_for_sidewalk_construction_permit() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.nextTablePage);
	    re.click(Byloc.CustomerPortal.nextTablePage, "Next Page");
	    Thread.sleep(2000);
	    re.scrollTo(Byloc.CustomerPortal.applySidewalkConstructionBtn, driver);
	    re.click(Byloc.CustomerPortal.applySidewalkConstructionBtn, "Apply Application");
	}

	@And("^the user will fill up the form for sidewalk construction permit while leaving some fields blank$")
	public void the_user_will_fill_up_the_form_for_sidewalk_construction_permit_while_leaving_some_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkConstructionLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMiddleNameFld, "Midde Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionTelephoneNoFld, "Telephone No.", "5456678");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMobileNoFld, "Mobile No.", "09994465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionRepresentativeFld, "Authorized Representative", "John Doe");
	}

	@When("^the user submit the sidewalk construction permit form$")
	public void the_user_submit_the_sidewalk_construction_permit_form() throws Throwable {
		re.click(Byloc.CustomerPortal.sidewalkConstructionSubmitBtn, "Submit");
	    re.click(Byloc.confirmSubmit, "Submit");
	}

	@Then("^the submission of sidewalk construction permit form will not proceed$")
	public void the_submission_of_sidewalk_construction_permit_form_will_not_proceed() throws Throwable {
		the_submission_of_locational_clearance_form_will_not_proceed();
	}
	
	@And("^the user will fill up the form for sidewalk construction permit w/ invalid telephone no\\.$")
	public void the_user_will_fill_up_the_form_for_sidewalk_construction_permit_w_invalid_telephone_no() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkConstructionLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMiddleNameFld, "Midde Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionTelephoneNoFld, "Telephone No.", "545gdfhsyer6678");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMobileNoFld, "Mobile No.", "09994465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionBuildingFld, "Building", "BLDG. NO. 453");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionRepresentativeFld, "Authorized Representative", "John Doe");
	}
	
	@And("^the user will fill up the form for sidewalk construction permit w/ invalid mobile no\\.$")
	public void the_user_will_fill_up_the_form_for_sidewalk_construction_permit_w_invalid_mobile_no() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkConstructionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMiddleNameFld, "Midde Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionTelephoneNoFld, "Telephone No.", "5456678");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMobileNoFld, "Mobile No.", "099dsgrwe4394465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionBuildingFld, "Building", "BLDG. NO. 453");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionRepresentativeFld, "Authorized Representative", "John Doe");
	}
	
	@And("^the user will fill up the form for sidewalk construction permit w/ invalid email$")
	public void the_user_will_fill_up_the_form_for_sidewalk_construction_permit_w_invalid_email() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkConstructionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMiddleNameFld, "Midde Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionTelephoneNoFld, "Telephone No.", "5456678");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMobileNoFld, "Mobile No.", "09994465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"emailcom";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionBuildingFld, "Building", "BLDG. NO. 453");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionRepresentativeFld, "Authorized Representative", "John Doe");
	}

	@When("^the submit button for sidewalk construction permit is clicked$")
	public void the_submit_button_for_sidewalk_construction_permit_is_clicked() throws Throwable {
		re.click(Byloc.CustomerPortal.sidewalkConstructionSubmitBtn, "Submit");
	}
	
	@And("^the user will fill up the form for sidewalk construction permit w/ valid and complete details$")
	public void the_user_will_fill_up_the_form_for_sidewalk_construction_permit_w_valid_and_complete_details() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkConstructionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMiddleNameFld, "Midde Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionTelephoneNoFld, "Telephone No.", "5456678");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMobileNoFld, "Mobile No.", "09994465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionBuildingFld, "Building", "BLDG. NO. 453");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionRepresentativeFld, "Authorized Representative", "John Doe");
	}
	
	@And("^click Apply Application button for wiring clearance$")
	public void click_Apply_Application_button_for_wiring_clearance() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.nextTablePage);
	    re.click(Byloc.CustomerPortal.nextTablePage, "Next Page");
	    el.waitVisible(Byloc.CustomerPortal.nextTablePage);
	    re.click(Byloc.CustomerPortal.nextTablePage, "Next Page");
	    re.click(Byloc.CustomerPortal.applyWiringClearanceBtn, "Apply Application");
	}

	@And("^the user will fill up the form for wiring clearance while leaving some fields blank$")
	public void the_user_will_fill_up_the_form_for_wiring_clearance_while_leaving_some_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.wiringLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.wiringLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.wiringSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.wiringAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringMobileNoFld, "Mobile No.", "09884465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.scrollTo(Byloc.CustomerPortal.wiringPurposeParent, driver);
	    re.getTextbox(Byloc.CustomerPortal.wiringLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringFloorFld, "Floor/Lot Area", ""+Util.randomNum(999, 111));
	}

	@When("^the user submit the wiring clearance form$")
	public void the_user_submit_the_wiring_clearance_form() throws Throwable {
		re.click(Byloc.CustomerPortal.wiringSubmitBtn, "Submit");
	    re.click(Byloc.confirmSubmit, "Submit");
	}

	@Then("^the submission of wiring clearance form will not proceed$")
	public void the_submission_of_wiring_clearance_form_will_not_proceed() throws Throwable {
		the_submission_of_locational_clearance_form_will_not_proceed();
	}
	
	@And("^the user will fill up the form for wiring clearance w/ invalid telephone number$")
	public void the_user_will_fill_up_the_form_for_wiring_clearance_w_invalid_telephone_number() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.wiringLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.wiringLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.wiringFirstNameFld, "First Name", "Karl");
	    re.getTextbox(Byloc.CustomerPortal.wiringMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.wiringSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.wiringAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringTelephoneNoFld, "Telephone No.", "5sdfs456678");
	    re.getTextbox(Byloc.CustomerPortal.wiringMobileNoFld, "Mobile No.", "09884465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.wiringEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.wiringPurposeParent, driver);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.wiringPurposeParent, Byloc.childRadioClassName);
	    re.getTextbox(Byloc.CustomerPortal.wiringLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringFloorFld, "Floor/Lot Area", ""+Util.randomNum(999, 111));
	}

	@When("^the submit button for wiring clearance is clicked$")
	public void the_submit_button_for_wiring_clearance_is_clicked() throws Throwable {
	    re.click(Byloc.CustomerPortal.wiringSubmitBtn, "Submit");
	}
	
	@And("^the user will fill up the form for wiring clearance w/ invalid mobile number$")
	public void the_user_will_fill_up_the_form_for_wiring_clearance_w_invalid_mobile_number() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.wiringLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.wiringLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.wiringFirstNameFld, "First Name", "Karl");
	    re.getTextbox(Byloc.CustomerPortal.wiringMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.wiringSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.wiringAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringTelephoneNoFld, "Telephone No.", "5456678");
	    re.getTextbox(Byloc.CustomerPortal.wiringMobileNoFld, "Mobile No.", "098844as6557asd8");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.wiringEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.wiringPurposeParent, driver);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.wiringPurposeParent, Byloc.childRadioClassName);
	    re.getTextbox(Byloc.CustomerPortal.wiringLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringFloorFld, "Floor/Lot Area", ""+Util.randomNum(999, 111));
	}
	
	@And("^the user will fill up the form for wiring clearance w/ invalid email$")
	public void the_user_will_fill_up_the_form_for_wiring_clearance_w_invalid_email() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.wiringLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.wiringLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.wiringFirstNameFld, "First Name", "Karl");
	    re.getTextbox(Byloc.CustomerPortal.wiringMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.wiringSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.wiringAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringTelephoneNoFld, "Telephone No.", "5456678");
	    re.getTextbox(Byloc.CustomerPortal.wiringMobileNoFld, "Mobile No.", "09884465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"emailcom";
	    re.getTextbox(Byloc.CustomerPortal.wiringEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.wiringPurposeParent, driver);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.wiringPurposeParent, Byloc.childRadioClassName);
	    re.getTextbox(Byloc.CustomerPortal.wiringLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringFloorFld, "Floor/Lot Area", ""+Util.randomNum(999, 111));
	}
	
	@And("^the user will fill up the form for wiring clearance w/ non-numerical value for floor/lot area field$")
	public void the_user_will_fill_up_the_form_for_wiring_clearance_w_non_numerical_value_for_floor_lot_area_field() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.wiringLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.wiringLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.wiringFirstNameFld, "First Name", "Karl");
	    re.getTextbox(Byloc.CustomerPortal.wiringMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.wiringSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.wiringAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringTelephoneNoFld, "Telephone No.", "5456678");
	    re.getTextbox(Byloc.CustomerPortal.wiringMobileNoFld, "Mobile No.", "09884465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.wiringEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.wiringPurposeParent, driver);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.wiringPurposeParent, Byloc.childRadioClassName);
	    re.getTextbox(Byloc.CustomerPortal.wiringLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringFloorFld, "Floor/Lot Area", "sdsd");
	}
	
	@And("^the user will fill up the form for wiring clearance w/ no selected purpose$")
	public void the_user_will_fill_up_the_form_for_wiring_clearance_w_no_selected_purpose() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.wiringLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.wiringLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.wiringFirstNameFld, "First Name", "Karl");
	    re.getTextbox(Byloc.CustomerPortal.wiringMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.wiringSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.wiringAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringTelephoneNoFld, "Telephone No.", "5456678");
	    re.getTextbox(Byloc.CustomerPortal.wiringMobileNoFld, "Mobile No.", "09884465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.wiringEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.wiringPurposeParent, driver);
	    re.getTextbox(Byloc.CustomerPortal.wiringLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringFloorFld, "Floor/Lot Area", ""+Util.randomNum(999, 111));
	}
	
	@And("^the user will fill up the form for wiring clearance w/ valid and complete details$")
	public void the_user_will_fill_up_the_form_for_wiring_clearance_w_valid_and_complete_details() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.wiringLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.wiringLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.wiringFirstNameFld, "First Name", "Karl");
	    re.getTextbox(Byloc.CustomerPortal.wiringMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.wiringSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.wiringAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringTelephoneNoFld, "Telephone No.", "5456678");
	    re.getTextbox(Byloc.CustomerPortal.wiringMobileNoFld, "Mobile No.", "09884465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.wiringEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.wiringPurposeParent, driver);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.wiringPurposeParent, Byloc.childRadioClassName);
	    re.getTextbox(Byloc.CustomerPortal.wiringLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringFloorFld, "Floor/Lot Area", ""+Util.randomNum(999, 111));
	}
	
	@And("^click Apply Application button for posting of streamer and tarpaulin$")
	public void click_Apply_Application_button_for_posting_of_streamer_and_tarpaulin() throws Throwable {
	    el.waitVisible(Byloc.CustomerPortal.nextTablePage);
	    re.click(Byloc.CustomerPortal.nextTablePage, "Next");
	    el.waitVisible(Byloc.CustomerPortal.applyPostingOfStreamerBtn);
	    re.click(Byloc.CustomerPortal.applyPostingOfStreamerBtn, "Apply Application");
	}

	@And("^then fill up the form for posting of streamer and tarpaulin leaving some fields blank$")
	public void the_fill_up_the_form_for_posting_of_streamer_and_tarpaulin_leaving_some_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.tarpLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.tarpFirstNameFld, "First Name", "Enrico");
		re.getTextbox(Byloc.CustomerPortal.tarpSuffixFld, "Suffix", "Sr.");
		re.getTextbox(Byloc.CustomerPortal.tarpAddressFld, "Address", "Mandaluyong, Metro Manila");
		re.getTextbox(Byloc.CustomerPortal.tarpTelephoneFld, "Telephone No.", "5456678");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.CustomerPortal.tarpEmailFld, "Email", email);
		re.scrollTo(Byloc.CustomerPortal.tarpAnnouncementFld, driver);
	}

	@When("^the posting of streamer and tarpaulin form is submitted$")
	public void the_posting_of_streamer_and_tarpaulin_form_is_submitted() throws Throwable {
		 re.click(Byloc.CustomerPortal.tarpSubmitBtn, "Submit");
		 re.click(Byloc.confirmSubmit, "Submit");
	}

	@Then("^the submission of posting of streamer and tarpaulin form will not proceed$")
	public void the_submission_of_posting_of_streamer_and_tarpaulin_form_will_not_proceed() throws Throwable {
		the_submission_of_locational_clearance_form_will_not_proceed();
	}
	
	@And("^then fill up the form for posting of streamer and tarpaulin leaving all fields blank$")
	public void the_fill_up_the_form_for_posting_of_streamer_and_tarpaulin_leaving_all_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.tarpLastNameFld); 
		re.scrollTo(Byloc.CustomerPortal.tarpSubmitBtn, driver);
	}
	
	@And("^then fill up the form for posting of streamer and tarpaulin w/ invalid email format$")
	public void the_fill_up_the_form_for_posting_of_streamer_and_tarpaulin_w_invalid_email_format() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.tarpLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.tarpLastNameFld, "Last Name", "Guevarra");
		re.getTextbox(Byloc.CustomerPortal.tarpFirstNameFld, "First Name", "Enrico");
		re.getTextbox(Byloc.CustomerPortal.tarpMiddleNameFld, "Middle Name", "Quizon");
		re.getTextbox(Byloc.CustomerPortal.tarpSuffixFld, "Suffix", "Sr.");
		re.getTextbox(Byloc.CustomerPortal.tarpAddressFld, "Address", "Mandaluyong, Metro Manila");
		re.getTextbox(Byloc.CustomerPortal.tarpTelephoneFld, "Telephone No.", "5456678");
		re.getTextbox(Byloc.CustomerPortal.tarpMobileFld, "Mobile No.", "09887789909");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"emailcom";
		re.getTextbox(Byloc.CustomerPortal.tarpEmailFld, "Email", email);
		re.scrollTo(Byloc.CustomerPortal.tarpAnnouncementFld, driver);
		re.getTextbox(Byloc.CustomerPortal.tarpAnnouncementFld, "Content/Announcement", "Sample content/announcement only!");
		re.getTextbox(Byloc.CustomerPortal.tarpScheduleFld, "Posting Schedule", "");
	}

	@When("^the submit button for posting of streamer and tarpaulin is clicked$")
	public void the_submit_button_for_posting_of_streamer_and_tarpaulin_is_clicked() throws Throwable {
		re.click(Byloc.CustomerPortal.tarpSubmitBtn, "Submit");
	}
	
	@And("^then fill up the form for posting of streamer and tarpaulin while using a tel\\. no\\. w/ non-numerical characters$")
	public void the_fill_up_the_form_for_posting_of_streamer_and_tarpaulin_while_using_a_tel_no_w_non_numerical_characters() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.tarpLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.tarpLastNameFld, "Last Name", "Guevarra");
		re.getTextbox(Byloc.CustomerPortal.tarpFirstNameFld, "First Name", "Enrico");
		re.getTextbox(Byloc.CustomerPortal.tarpMiddleNameFld, "Middle Name", "Quizon");
		re.getTextbox(Byloc.CustomerPortal.tarpSuffixFld, "Suffix", "Sr.");
		re.getTextbox(Byloc.CustomerPortal.tarpAddressFld, "Address", "Mandaluyong, Metro Manila");
		re.getTextbox(Byloc.CustomerPortal.tarpTelephoneFld, "Telephone No.", "545sdfsdf6678");
		re.getTextbox(Byloc.CustomerPortal.tarpMobileFld, "Mobile No.", "09887789909");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.CustomerPortal.tarpEmailFld, "Email", email);
		re.scrollTo(Byloc.CustomerPortal.tarpAnnouncementFld, driver);
		re.getTextbox(Byloc.CustomerPortal.tarpAnnouncementFld, "Content/Announcement", "Sample content/announcement only!");
		re.getTextbox(Byloc.CustomerPortal.tarpScheduleFld, "Posting Schedule", "");
	}

	@And("^then fill up the form for posting of streamer and tarpaulin while using a tel\\.no\\. w/ invalid length$")
	public void the_fill_up_the_form_for_posting_of_streamer_and_tarpaulin_while_using_a_tel_no_w_invalid_length() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.tarpLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.tarpLastNameFld, "Last Name", "Guevarra");
		re.getTextbox(Byloc.CustomerPortal.tarpFirstNameFld, "First Name", "Enrico");
		re.getTextbox(Byloc.CustomerPortal.tarpMiddleNameFld, "Middle Name", "Quizon");
		re.getTextbox(Byloc.CustomerPortal.tarpSuffixFld, "Suffix", "Sr.");
		re.getTextbox(Byloc.CustomerPortal.tarpAddressFld, "Address", "Mandaluyong, Metro Manila");
		re.getTextbox(Byloc.CustomerPortal.tarpTelephoneFld, "Telephone No.", "5453452434645645756678");
		re.getTextbox(Byloc.CustomerPortal.tarpMobileFld, "Mobile No.", "09887789909");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.CustomerPortal.tarpEmailFld, "Email", email);
		re.scrollTo(Byloc.CustomerPortal.tarpAnnouncementFld, driver);
		re.getTextbox(Byloc.CustomerPortal.tarpAnnouncementFld, "Content/Announcement", "Sample content/announcement only!");
		re.getTextbox(Byloc.CustomerPortal.tarpScheduleFld, "Posting Schedule", "");
	}
	
	@And("^then fill up the form for posting of streamer and tarpaulin w/ a mobile no\\. w/ non-numerical characters$")
	public void the_fill_up_the_form_for_posting_of_streamer_and_tarpaulin_w_a_mobile_no_w_non_numerical_characters() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.tarpLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.tarpLastNameFld, "Last Name", "Guevarra");
		re.getTextbox(Byloc.CustomerPortal.tarpFirstNameFld, "First Name", "Enrico");
		re.getTextbox(Byloc.CustomerPortal.tarpMiddleNameFld, "Middle Name", "Quizon");
		re.getTextbox(Byloc.CustomerPortal.tarpSuffixFld, "Suffix", "Sr.");
		re.getTextbox(Byloc.CustomerPortal.tarpAddressFld, "Address", "Mandaluyong, Metro Manila");
		re.getTextbox(Byloc.CustomerPortal.tarpTelephoneFld, "Telephone No.", "5456788");
		re.getTextbox(Byloc.CustomerPortal.tarpMobileFld, "Mobile No.", "0988778erwerwer9909");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.CustomerPortal.tarpEmailFld, "Email", email);
		re.scrollTo(Byloc.CustomerPortal.tarpAnnouncementFld, driver);
		re.getTextbox(Byloc.CustomerPortal.tarpAnnouncementFld, "Content/Announcement", "Sample content/announcement only!");
		re.getTextbox(Byloc.CustomerPortal.tarpScheduleFld, "Posting Schedule", "");
	}

	@And("^then fill up the form for posting of streamer and tarpaulin w/ a mobile no\\. w/ invalid length$")
	public void the_fill_up_the_form_for_posting_of_streamer_and_tarpaulin_w_a_mobile_no_w_invalid_length() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.tarpLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.tarpLastNameFld, "Last Name", "Guevarra");
		re.getTextbox(Byloc.CustomerPortal.tarpFirstNameFld, "First Name", "Enrico");
		re.getTextbox(Byloc.CustomerPortal.tarpMiddleNameFld, "Middle Name", "Quizon");
		re.getTextbox(Byloc.CustomerPortal.tarpSuffixFld, "Suffix", "Sr.");
		re.getTextbox(Byloc.CustomerPortal.tarpAddressFld, "Address", "Mandaluyong, Metro Manila");
		re.getTextbox(Byloc.CustomerPortal.tarpTelephoneFld, "Telephone No.", "5456788");
		re.getTextbox(Byloc.CustomerPortal.tarpMobileFld, "Mobile No.", "098877899093333333333");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.CustomerPortal.tarpEmailFld, "Email", email);
		re.scrollTo(Byloc.CustomerPortal.tarpAnnouncementFld, driver);
		re.getTextbox(Byloc.CustomerPortal.tarpAnnouncementFld, "Content/Announcement", "Sample content/announcement only!");
		re.getTextbox(Byloc.CustomerPortal.tarpScheduleFld, "Posting Schedule", "");
	}

	@And("^then fill up the form for posting of streamer and tarpaulin w/ valid and complete details$")
	public void the_fill_up_the_form_for_posting_of_streamer_and_tarpaulin_w_valid_and_complete_details() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.tarpLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.tarpLastNameFld, "Last Name", "Guevarra");
		re.getTextbox(Byloc.CustomerPortal.tarpFirstNameFld, "First Name", "Enrico");
		re.getTextbox(Byloc.CustomerPortal.tarpMiddleNameFld, "Middle Name", "Quizon");
		re.getTextbox(Byloc.CustomerPortal.tarpSuffixFld, "Suffix", "Sr.");
		re.getTextbox(Byloc.CustomerPortal.tarpAddressFld, "Address", "Mandaluyong, Metro Manila");
		re.getTextbox(Byloc.CustomerPortal.tarpTelephoneFld, "Telephone No.", "5456788");
		re.getTextbox(Byloc.CustomerPortal.tarpMobileFld, "Mobile No.", "09887789909");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.CustomerPortal.tarpEmailFld, "Email", email);
		re.scrollTo(Byloc.CustomerPortal.tarpAnnouncementFld, driver);
		re.getTextbox(Byloc.CustomerPortal.tarpAnnouncementFld, "Content/Announcement", "Sample content/announcement only!");
		re.getTextbox(Byloc.CustomerPortal.tarpScheduleFld, "Posting Schedule", "");
	}

	@And("^the user submit the posting of streamer and tarpaulin form$")
	public void the_user_submit_the_posting_of_streamer_and_tarpaulin_form() throws Throwable {
		the_posting_of_streamer_and_tarpaulin_form_is_submitted();
	}

	@And("^then fill up the form for posting of streamer and tarpaulin w/o the content/announcement$")
	public void the_fill_up_the_form_for_posting_of_streamer_and_tarpaulin_w_o_the_content_announcement() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.tarpLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.tarpLastNameFld, "Last Name", "Guevarra");
		re.getTextbox(Byloc.CustomerPortal.tarpFirstNameFld, "First Name", "Enrico");
		re.getTextbox(Byloc.CustomerPortal.tarpMiddleNameFld, "Middle Name", "Quizon");
		re.getTextbox(Byloc.CustomerPortal.tarpSuffixFld, "Suffix", "Sr.");
		re.getTextbox(Byloc.CustomerPortal.tarpAddressFld, "Address", "Mandaluyong, Metro Manila");
		re.getTextbox(Byloc.CustomerPortal.tarpTelephoneFld, "Telephone No.", "5456788");
		re.getTextbox(Byloc.CustomerPortal.tarpMobileFld, "Mobile No.", "09887789909");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.CustomerPortal.tarpEmailFld, "Email", email);
		re.scrollTo(Byloc.CustomerPortal.tarpAnnouncementFld, driver);
		re.getTextbox(Byloc.CustomerPortal.tarpScheduleFld, "Posting Schedule", "");
	}

	@And("^then fill up the form for posting of streamer and tarpaulin w/o the posting schedule$")
	public void the_fill_up_the_form_for_posting_of_streamer_and_tarpaulin_w_o_the_posting_schedule() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.tarpLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.tarpLastNameFld, "Last Name", "Guevarra");
		re.getTextbox(Byloc.CustomerPortal.tarpFirstNameFld, "First Name", "Enrico");
		re.getTextbox(Byloc.CustomerPortal.tarpMiddleNameFld, "Middle Name", "Quizon");
		re.getTextbox(Byloc.CustomerPortal.tarpSuffixFld, "Suffix", "Sr.");
		re.getTextbox(Byloc.CustomerPortal.tarpAddressFld, "Address", "Mandaluyong, Metro Manila");
		re.getTextbox(Byloc.CustomerPortal.tarpTelephoneFld, "Telephone No.", "5456788");
		re.getTextbox(Byloc.CustomerPortal.tarpMobileFld, "Mobile No.", "09887789909");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.CustomerPortal.tarpEmailFld, "Email", email);
		re.scrollTo(Byloc.CustomerPortal.tarpAnnouncementFld, driver);
		re.getTextbox(Byloc.CustomerPortal.tarpAnnouncementFld, "Content/Announcement", "Sample content/announcement only!");
	}
	
	@And("^then fill up the form for excavation clearance leaving all fields blank$")
	public void the_fill_up_the_form_for_excavation_clearance_leaving_all_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.excavationSubmit);
		re.scrollTo(Byloc.CustomerPortal.excavationSubmit, driver);
	}

	@And("^then fill up the form for excavation clearance leaving the fields for middle name and suffix blank$")
	public void the_fill_up_the_form_for_excavation_clearance_leaving_the_fields_for_middle_name_and_suffix_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.excavationCompanyNameFld);
		re.getTextbox(Byloc.CustomerPortal.excavationCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.excavationCompanyAddressFld, "Company Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationTelNoFld, "Telephone Number", "5459655");
	    re.getTextbox(Byloc.CustomerPortal.excavationMobileNoFld, "Mobile Number", "09888675589");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.excavationEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.excavationPurposeFld, "Purpose", "Purpose");
	    re.scrollTo(Byloc.CustomerPortal.excavationLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.excavationLocationFld, "Exact Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationRepresentativeFld, "Authorized Representative", "John Doe");
	    re.getTextbox(Byloc.CustomerPortal.excavationLastNameFld, "Lasst Name", "Villareal");	
	    re.getTextbox(Byloc.CustomerPortal.excavationFirstNameFld, "First Name", "Angelo");
	}
	
	@And("^then fill up the form for excavation clearance w/ mobile number w/ non-numerical characters$")
	public void then_fill_up_the_form_for_excavation_clearance_w_mobile_number_w_non_numerical_characters() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.excavationCompanyNameFld);
		re.getTextbox(Byloc.CustomerPortal.excavationCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.excavationCompanyAddressFld, "Company Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationTelNoFld, "Telephone Number", "5459655");
	    re.getTextbox(Byloc.CustomerPortal.excavationMobileNoFld, "Mobile Number", "098886gdgdg75589");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.excavationEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.excavationPurposeFld, "Purpose", "Purpose");
	    re.scrollTo(Byloc.CustomerPortal.excavationLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.excavationLocationFld, "Exact Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.excavationRepresentativeFld, "Authorized Representative", "John Doe");
	    re.getTextbox(Byloc.CustomerPortal.excavationLastNameFld, "Lasst Name", "Villareal");	
	    re.getTextbox(Byloc.CustomerPortal.excavationFirstNameFld, "First Name", "Angelo");
	    re.getTextbox(Byloc.CustomerPortal.excavationMiddleNameFld, "Middle Name", "Aurelea");
	    re.getTextbox(Byloc.CustomerPortal.excavationSuffixFld, "Suffix", "N/A");
	}
	
	@And("^then fill up the form for locational clearance leaving all fields blank$")
	public void then_fill_up_the_form_for_locational_clearance_leaving_all_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.submitLocationalBtn); 
		re.scrollTo(Byloc.CustomerPortal.submitLocationalBtn, driver);
	}

	@And("^then fill up the form for locational clearance w/ a mobile no\\. w/ non-numerical characters$")
	public void then_fill_up_the_form_for_locational_clearance_w_a_mobile_no_w_non_numerical_characters() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.locationalLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.locationalLastNameFld , "Last name", "Guevarra");
	    re.getTextbox(Byloc.CustomerPortal.locationalFirstNameFld , "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.locationalMiddleNameFld , "Middle name", "Mendrano");
	    re.getTextbox(Byloc.CustomerPortal.locationalSuffixFld , "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.locationalAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalTelNoFld, "Telephone no.", "5456788");
	    re.getTextbox(Byloc.CustomerPortal.locationalMobileNoFld, "Mobile no.", "098845ss6"+Util.randomNum(9999, 1111));
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.locationalEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.locationalBusinessNameFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessNameFld, "Business name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessLocationFld, "Business location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalLineOfBusinessFld, "Line of Business", "Mobile Money");
	}

	@And("^then fill up the form for locational clearance w/ a telephone no\\. w/ non-numerical values$")
	public void then_fill_up_the_form_for_locational_clearance_w_a_telephone_no_w_non_numerical_values() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.locationalLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.locationalLastNameFld , "Last name", "Guevarra");
	    re.getTextbox(Byloc.CustomerPortal.locationalFirstNameFld , "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.locationalMiddleNameFld , "Middle name", "Mendrano");
	    re.getTextbox(Byloc.CustomerPortal.locationalSuffixFld , "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.locationalAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalTelNoFld, "Telephone no.", "5456aser788");
	    re.getTextbox(Byloc.CustomerPortal.locationalMobileNoFld, "Mobile no.", "0988456"+Util.randomNum(9999, 1111));
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.locationalEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.locationalBusinessNameFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessNameFld, "Business name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.locationalBusinessLocationFld, "Business location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.locationalLineOfBusinessFld, "Line of Business", "Mobile Money");
	}
	
	@And("^the user will fill up the form for sidewalk construction permit while leaving all fields blank$")
	public void the_user_will_fill_up_the_form_for_sidewalk_construction_permit_while_leaving_all_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkConstructionSubmitBtn); 
		re.scrollTo(Byloc.CustomerPortal.sidewalkConstructionSubmitBtn, driver);
	}

	@And("^the user will fill up the form for sidewalk construction permit w/ a telephone number w/ invalid length$")
	public void the_user_will_fill_up_the_form_for_sidewalk_construction_permit_w_a_telephone_number_w_invalid_length() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkConstructionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMiddleNameFld, "Midde Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionTelephoneNoFld, "Telephone No.", "53333333456678");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMobileNoFld, "Mobile No.", "09994465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionBuildingFld, "Building", "BLDG. NO. 453");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionRepresentativeFld, "Authorized Representative", "John Doe");
	}

	@And("^the user will fill up the form for sidewalk construction permit w/ a mobile no\\. w/ invalid length$")
	public void the_user_will_fill_up_the_form_for_sidewalk_construction_permit_w_a_mobile_no_w_invalid_length() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkConstructionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMiddleNameFld, "Midde Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionTelephoneNoFld, "Telephone No.", "5456678");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionMobileNoFld, "Mobile No.", "09994465666666578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionBuildingFld, "Building", "BLDG. NO. 453");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkConstructionRepresentativeFld, "Authorized Representative", "John Doe");
	}
	
	@And("^the user will fill up the form for enclosure permit while leaving all fields blank$")
	public void the_user_will_fill_up_the_form_for_enclosure_permit_while_leaving_all_fields_blank() throws Throwable {
		el.waitAllVisible(Byloc.CustomerPortal.sidewalkConstructionSubmitBtn);
		re.scrollTo(Byloc.CustomerPortal.sidewalkSubmitBtn, driver);
	}

	@And("^the user will fill up the form for enclosure permit w/ a mobile number w/ invalid length$")
	public void the_user_will_fill_up_the_form_for_enclosure_permit_w_a_mobile_number_w_invalid_length() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkCompanyNameFld);
		re.getTextbox(Byloc.CustomerPortal.sidewalkCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkTelNoFld, "Telephone No.", "5456788");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkMobileNoFld, "Mobile Number", "0988888887767789");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkFrontageFld, "Frontage", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkWidthFld, "Width", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkSquareMeterFld, "Square Meter(s)", ""+Util.generateRandomNumber(999, 111));
	    re.scrollTo(Byloc.CustomerPortal.sidewalkLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkLocationFld, "Construction Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkPeriodFld, "Construction Period", "May 11 to Aug 11 2021");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkRepresentativeFld, "Authorized Representative", "Sample Name");
	}

	@And("^the user will fill up the form for enclosure permit w/ a telephone no\\. w/ invalid length$")
	public void the_user_will_fill_up_the_form_for_enclosure_permit_w_a_telephone_no_w_invalid_length() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.sidewalkCompanyNameFld);
		re.getTextbox(Byloc.CustomerPortal.sidewalkCompanyNameFld, "Company Name", "TLC");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkTelNoFld, "Telephone No.", "5456788");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkMobileNoFld, "Mobile Number", "0980980798798787767789");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.sidewalkEmailFld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkFrontageFld, "Frontage", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkWidthFld, "Width", ""+Util.generateRandomNumber(999, 111));
	    re.getTextbox(Byloc.CustomerPortal.sidewalkSquareMeterFld, "Square Meter(s)", ""+Util.generateRandomNumber(999, 111));
	    re.scrollTo(Byloc.CustomerPortal.sidewalkLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.sidewalkLocationFld, "Construction Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkPeriodFld, "Construction Period", "May 11 to Aug 11 2021");
	    re.getTextbox(Byloc.CustomerPortal.sidewalkRepresentativeFld, "Authorized Representative", "Sample Name");
	}
	
	@And("^the user will fill up the form for wiring clearance while leaving all fields blank$")
	public void the_user_will_fill_up_the_form_for_wiring_clearance_while_leaving_all_fields_blank() throws Throwable {
		el.waitAllVisible(Byloc.CustomerPortal.wiringSubmitBtn);
		re.scrollTo(Byloc.CustomerPortal.wiringSubmitBtn, driver);
	}

	@And("^the user will fill up the form for wiring clearance w/ a telephone number w/ invalid length$")
	public void the_user_will_fill_up_the_form_for_wiring_clearance_w_a_telephone_number_w_invalid_length() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.wiringLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.wiringLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.wiringFirstNameFld, "First Name", "Karl");
	    re.getTextbox(Byloc.CustomerPortal.wiringMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.wiringSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.wiringAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringTelephoneNoFld, "Telephone No.", "5455555556678");
	    re.getTextbox(Byloc.CustomerPortal.wiringMobileNoFld, "Mobile No.", "09884465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.wiringEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.wiringPurposeParent, driver);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.wiringPurposeParent, Byloc.childRadioClassName);
	    re.getTextbox(Byloc.CustomerPortal.wiringLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringFloorFld, "Floor/Lot Area", ""+Util.randomNum(999, 111));
	}

	@And("^the user will fill up the form for wiring clearance w/ a mobile no\\. w/ invalid length$")
	public void the_user_will_fill_up_the_form_for_wiring_clearance_w_a_mobile_no_w_invalid_length() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.wiringLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.wiringLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.wiringFirstNameFld, "First Name", "Karl");
	    re.getTextbox(Byloc.CustomerPortal.wiringMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.wiringSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.wiringAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringTelephoneNoFld, "Telephone No.", "5456678");
	    re.getTextbox(Byloc.CustomerPortal.wiringMobileNoFld, "Mobile No.", "0985675675867884465578");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.wiringEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.wiringPurposeParent, driver);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.wiringPurposeParent, Byloc.childRadioClassName);
	    re.getTextbox(Byloc.CustomerPortal.wiringLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.wiringFloorFld, "Floor/Lot Area", ""+Util.randomNum(999, 111));
	}
	
	@And("^click Apply Application button for scaffolding permit$")
	public void click_Apply_Application_button_for_scaffolding_permit() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.nextTablePage);
		re.click(Byloc.CustomerPortal.nextTablePage, "Next Page");
		el.waitVisible(Byloc.CustomerPortal.nextTablePage);
		re.scrollTo(Byloc.CustomerPortal.applyScaffoldingPermitBtn, driver);
		re.click(Byloc.CustomerPortal.applyScaffoldingPermitBtn, "Apply Application");
	}

	@And("^then fill up the form for scaffolding permit leaving all fields blank$")
	public void then_fill_up_the_form_for_scaffolding_permit_leaving_all_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.scaffoldingLastNameFld);
		Thread.sleep(3000);
		re.scrollTo(Byloc.CustomerPortal.scaffoldingSubmitBtn, driver);
	}

	@When("^the submit button for scaffolding permit is clicked$")
	public void the_submit_button_for_scaffolding_permit_is_clicked() throws Throwable {
	    re.click(Byloc.CustomerPortal.scaffoldingSubmitBtn, "Submit");
	}

	@Then("^a required validation under scaffolding permit application fields will appear$")
	public void a_required_validation_under_scaffolding_permit_application_fields_will_appear() throws Throwable {
	    Thread.sleep(2000);
	    boolean con = driver.findElements(Byloc.requiredLbl).isEmpty();
	    Assert.assertEquals(false, con);
	}
	
	@And("^then fill up the form for scaffolding permit leaving the middle name and suffix field empty$")
	public void then_fill_up_the_form_for_scaffolding_permit_leaving_the_middle_name_and_suffix_field_empty() throws Throwable {
	    el.waitVisible(Byloc.CustomerPortal.scaffoldingLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLastNameFld, "Last Name", "Montalban");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFirstNameFld, "First Name", "Gianna");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingBuildingFld, "Building", "One Gateway Residence");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingTelephoneFld, "Telephone No.", "2342342");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingMobileFld, "Mobile No.", "9876475869");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.scaffoldingLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFrontageFld, "Frontage", "123");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingPeriodStartFld, "Period (Start)", "03272021");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingPeriodEndFld, "Period (End)", "06272021");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingRepresentativeFld, "Authorized Representative", "John Doe");
	}

	@And("^the user submit the scaffolding permit form$")
	public void the_user_submit_the_scaffolding_permit_form() throws Throwable {
	    re.click(Byloc.CustomerPortal.scaffoldingSubmitBtn, "Submit");
	    re.click(Byloc.confirmSubmit, "Confirm Submit");
	}
	
	@When("^the user fills up the form for scaffolding permit and provided a telephone no\\.w/ invalid length$")
	public void the_user_fills_up_the_form_for_scaffolding_permit_and_provided_a_telephone_no_w_invalid_length() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.scaffoldingLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLastNameFld, "Last Name", "Montalban");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFirstNameFld, "First Name", "Gianna");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingBuildingFld, "Building", "One Gateway Residence");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingTelephoneFld, "Telephone No.", "23442");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingMobileFld, "Mobile No.", "9876475869");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.scaffoldingLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFrontageFld, "Frontage", "123");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingPeriodStartFld, "Period (Start)", "03272021");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingPeriodEndFld, "Period (End)", "06272021");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingRepresentativeFld, "Authorized Representative", "John Doe");
	}

	@Then("^an Invalid Format error message will appear under the telephone no\\.field$")
	public void an_Invalid_Format_error_message_will_appear_under_the_telephone_no_field() throws Throwable {
	    Thread.sleep(2000);
	    boolean con = driver.findElements(Byloc.invalidFormatTelephoneError).isEmpty();
	    Assert.assertEquals(false, con);
	}
	
	@When("^the user fills up the form for scaffolding permit and provided a mobile no\\.w/ invalid length$")
	public void the_user_fills_up_the_form_for_scaffolding_permit_and_provided_a_mobile_no_w_invalid_length() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.scaffoldingLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLastNameFld, "Last Name", "Montalban");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFirstNameFld, "First Name", "Gianna");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingBuildingFld, "Building", "One Gateway Residence");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingTelephoneFld, "Telephone No.", "2342342");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingMobileFld, "Mobile No.", "9876479");
	    re.scrollTo(Byloc.CustomerPortal.scaffoldingLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFrontageFld, "Frontage", "123");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingPeriodStartFld, "Period (Start)", "03272021");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingPeriodEndFld, "Period (End)", "06272021");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingRepresentativeFld, "Authorized Representative", "John Doe");
	}

	@Then("^an Invalid Format error message will appear under the mobile no\\.field$")
	public void an_Invalid_Format_error_message_will_appear_under_the_mobile_no_field() throws Throwable {
		Thread.sleep(2000);
	    boolean con = driver.findElements(Byloc.invalidFormatMobileError).isEmpty();
	    Assert.assertEquals(false, con);
	}
	
	@When("^the user fills up the form for scaffolding permit and provided a telephone no\\.w/ non-numerical characters$")
	public void the_user_fills_up_the_form_for_scaffolding_permit_and_provided_a_telephone_no_w_non_numerical_characters() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.scaffoldingLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLastNameFld, "Last Name", "Montalban");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFirstNameFld, "First Name", "Gianna");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingBuildingFld, "Building", "One Gateway Residence");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingTelephoneFld, "Telephone No.", no1+"sa"+no2);
	}

	@Then("^the non-numerical characters will not be accepted by the telephone no\\. field$")
	public void the_non_numerical_characters_will_not_be_accepted_by_the_telephone_no_field() throws Throwable {
	    String tel = re.getText(Byloc.CustomerPortal.scaffoldingTelephoneFld, "Telephone No.");
	    boolean con = tel.equalsIgnoreCase(no1+"sa"+no2);
	    Assert.assertEquals(false, con);
	}

	@When("^the user fills up the form for scaffolding permit and provided a mobile no\\.w/ non-numerical characters$")
	public void the_user_fills_up_the_form_for_scaffolding_permit_and_provided_a_mobile_no_w_non_numerical_characters() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.scaffoldingLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLastNameFld, "Last Name", "Montalban");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFirstNameFld, "First Name", "Gianna");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingBuildingFld, "Building", "One Gateway Residence");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingTelephoneFld, "Telephone No.", "5456788");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingMobileFld, "Mobile No.", no3+"as"+no4);
	}

	@Then("^the non-numerical characters will not be accepted by the mobile no\\. field$")
	public void the_non_numerical_characters_will_not_be_accepted_by_the_mobile_no_field() throws Throwable {
		 String tel = re.getText(Byloc.CustomerPortal.scaffoldingTelephoneFld, "Telephone No.");
		 boolean con = tel.equalsIgnoreCase("+63"+no3+"as"+no4);
		 Assert.assertEquals(false, con);
	}
	
	@And("^the user will fill up the form for scaffolding permit leaving the email field blank$")
	public void the_user_will_fill_up_the_form_for_scaffolding_permit_leaving_the_email_field_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.scaffoldingLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLastNameFld, "Last Name", "Montalban");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFirstNameFld, "First Name", "Gianna");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingBuildingFld, "Building", "One Gateway Residence");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingTelephoneFld, "Telephone No.", "2342342");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingMobileFld, "Mobile No.", "9876475869");
//	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
//	    re.getTextbox(Byloc.CustomerPortal.scaffoldingEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.scaffoldingLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFrontageFld, "Frontage", "123");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingPeriodStartFld, "Period (Start)", "03272021");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingPeriodEndFld, "Period (End)", "06272021");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingRepresentativeFld, "Authorized Representative", "John Doe");
	}

	@Then("^A required validation under the email field will appear$")
	public void a_required_validation_under_the_email_field_will_appear() throws Throwable {
	    boolean con = driver.findElements(Byloc.CustomerPortal.emailError).isEmpty();
	    if(!con) {
	    	String errorMsg = re.getText(Byloc.CustomerPortal.emailError, "Error message");
	    	con = errorMsg.equalsIgnoreCase("This field is required.");
	    }
	    Assert.assertEquals( true, con);
	}
	
	@When("^the user fills up the form for scaffolding permit w/ an email with invalid format$")
	public void the_user_fills_up_the_form_for_scaffolding_permit_w_an_email_with_invalid_format() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.scaffoldingLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLastNameFld, "Last Name", "Montalban");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFirstNameFld, "First Name", "Gianna");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingBuildingFld, "Building", "One Gateway Residence");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingTelephoneFld, "Telephone No.", "2342342");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingMobileFld, "Mobile No.", "9876475869");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"emailcom";
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.scaffoldingEmailFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFrontageFld, "Frontage", "123");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingPeriodStartFld, "Period (Start)", "03272021");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingPeriodEndFld, "Period (End)", "06272021");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingRepresentativeFld, "Authorized Representative", "John Doe");
	}

	@Then("^A Please enter a valid email address message will appear under the email field$")
	public void a_Please_enter_a_valid_email_address_message_will_appear_under_the_email_field() throws Throwable {
		boolean con = driver.findElements(Byloc.CustomerPortal.emailError).isEmpty();
	    if(!con) {
	    	String errorMsg = re.getText(Byloc.CustomerPortal.emailError, "Error message");
	    	con = errorMsg.equalsIgnoreCase("Please enter a valid email address.");
	    }
	    Assert.assertEquals( true, con);
	}
	
	@And("^the user clicked the submit button for scaffolding permit$")
	public void the_user_clicked_the_submit_button_for_scaffolding_permit() throws Throwable {
		the_submit_button_for_scaffolding_permit_is_clicked();
	}
	
	@And("^the user will fill up the form for scaffolding permit w/ only the middle name and suffix and leaving the other fields blank$")
	public void the_user_will_fill_up_the_form_for_scaffolding_permit_w_only_the_middle_name_and_suffix_and_leaving_the_other_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.scaffoldingLastNameFld);
		re.scrollTo(Byloc.CustomerPortal.scaffoldingSubmitBtn, driver);
	}
	
	@And("^then fill up the form for scaffolding permit w/ valid and complete details$")
	public void then_fill_up_the_form_for_scaffolding_permit_w_valid_and_complete_details() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.scaffoldingLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLastNameFld, "Last Name", "Montalban");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFirstNameFld, "First Name", "Gianna");
		re.getTextbox(Byloc.CustomerPortal.scaffoldingMiddleNameFld, "Middle Name", "Mirrana");
		re.getTextbox(Byloc.CustomerPortal.scaffoldingSuffixFld, "Suffix", "III");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingBuildingFld, "Building", "One Gateway Residence");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingTelephoneFld, "Telephone No.", "2342342");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingMobileFld, "Mobile No.", "9876475869");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.scaffoldingLocationFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingLocationFld, "Location", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingFrontageFld, "Frontage", "123");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingPeriodStartFld, "Period (Start)", "03272021");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingPeriodEndFld, "Period (End)", "06272021");
	    re.getTextbox(Byloc.CustomerPortal.scaffoldingRepresentativeFld, "Authorized Representative", "John Doe");
	}
}
