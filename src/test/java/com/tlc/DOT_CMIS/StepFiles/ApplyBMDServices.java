package com.tlc.DOT_CMIS.StepFiles;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.selenium.helper.Elements;
import com.selenium.helper.Util;
import com.tlc.DOT_CMIS.SharedClass;
import com.tlc.DOT_CMIS.Helpers.Byloc;
import com.tlc.DOT_CMIS.Helpers.Reuse;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;

public class ApplyBMDServices {
	
	WebDriver driver;
	Elements el;
	static Reuse re;
	
	String url = "http://119.8.63.161:81/Dashboard";
	String url2 = "http://119.8.63.161:84/dashboard";
	
	String email = "";
	String tel = "545941";

	public ApplyBMDServices(SharedClass sharedClass) {
		driver = sharedClass.setup();
		el = new Elements(driver, 3);
		re = new Reuse(driver);
	}
	
	@After
	public void tearDown() {
		this.driver.manage().deleteAllCookies();
		this.driver.quit();
		this.driver = null;
	}
	
	@Given("^That user will go to the customer portal$")
	public void that_user_will_go_to_the_customer_portal() throws Throwable {
		driver.manage().window().maximize();
		driver.get(url);
	}
	
	@And("^then click the Apply BMD Services menu$")
	public void then_click_the_Apply_BMD_Services_menu() throws Throwable {
	    re.click(Byloc.CustomerPortal.applyBMDLnk, "Apply BMD Services");
	}

	@And("^then click Apply Application button for Commercial Shooting Permit$")
	public void then_click_Apply_Application_button_for_Commercial_Shooting_Permit() throws Throwable {
	    re.click(Byloc.CustomerPortal.applyCommercialBtn, "Apply Application");
	}

	@And("^then fill up the form for Commercial Shooting Permit w/ valid and complete details$")
	public void then_fill_up_the_form_for_Commercial_Shooting_Permit_w_valid_and_complete_details() throws Throwable {
	    re.getTextbox(Byloc.CustomerPortal.lastNameFld, "Last name", "Buenavista");
	    re.getTextbox(Byloc.CustomerPortal.firstNameFld, "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.middleNameFld, "Middle name", "Rivero");
	    re.getTextbox(Byloc.CustomerPortal.suffixFld, "Suffix", "Jr.");
	    re.getTextbox(Byloc.CustomerPortal.addressFld, "Address", "Mandaluyong,Metro Manila");
	    re.scrollTo(Byloc.CustomerPortal.telephoneNumberfld, driver);
	    re.getTextbox(Byloc.CustomerPortal.telephoneNumberfld, "Telephone No.", "5459788");
	    re.getTextbox(Byloc.CustomerPortal.mobileNumberfld, "Mobile no.", "0977576"+Util.randomNum(9999, 1111));
	    re.getTextbox(Byloc.CustomerPortal.companyfld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.emailfld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.designationfld, "Designation", "Business Analyst");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.purposeParent, Byloc.childClassName);
	    re.click(Byloc.CustomerPortal.acomplishedShootFormChckbx, "Accomplsihed Details of Shoot Form");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.locationParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.shootingDateTimeFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.shootingDateTimeFld, "Date and Time of Shooting", "05090020211200AM");
	    re.getTextbox(Byloc.CustomerPortal.shootingHoursFld, "No.of Shooting Hours", ""+Util.randomNum(24, 3));
	    re.click(Byloc.CustomerPortal.withDroneChck, "With usage of drone");
	}

	@And("^the form for the Commercial Shooting Permit will be submitted$")
	public void the_form_for_the_Commercial_Shooting_Permit_will_be_submitted() throws Throwable {
	    re.click(Byloc.CustomerPortal.submitCommercialBtn, "Submit");
	    re.click(Byloc.confirmSubmit, "Confirm submit");
	    Thread.sleep(7000);
	}

	@And("^the user will go to the dashboard page$")
	public void the_user_will_go_to_the_dashboard_page() throws Throwable {
		this.driver.manage().deleteAllCookies();
		this.driver.close();
		
		SharedClass sharedClass = new SharedClass();
		driver = sharedClass.setup();
		el = new Elements(driver, 3);
		re = new Reuse(driver);
		
		driver.manage().window().maximize();
		driver.get(url2);
	}

	@And("^click the Permits an Clearances menu$")
	public void click_the_Permits_an_Clearances_menu() throws Throwable {
	    re.click(Byloc.BMDPortal.permitsAndClearancesMenu, "Permits and Clearances");
	}

	@And("^then click the Pending Applications menu$")
	public void then_click_the_Pending_Applications_menu() throws Throwable {
	    re.click(Byloc.BMDPortal.pendingApplicationsMenu, "Pending Applications");
	}

	@When("^the user goes to the last part of the table of for permits$")
	public void the_user_goes_to_the_last_part_of_the_table_of_for_permits() throws Throwable {
		re.click(Byloc.BMDPortal.tableLastPageBtn, "Last part of the table");
	}

	@Then("^the submitted application for commercial photography, videos and documentaries and shooting movies should be present$")
	public void the_submitted_application_for_commercial_photography_videos_and_documentaries_and_shooting_movies_should_be_present() throws Throwable {
		Thread.sleep(7000);
	    Util.print(email);
	    if(!driver.findElements(Byloc.BMDPortal.footer).isEmpty()) {
		    re.scrollTo(Byloc.BMDPortal.footer, driver);
	    }
	    Thread.sleep(2000);
	    boolean con = driver.findElements(By.xpath("//*[contains(text(), '"+email+"')]")).isEmpty();
	    Assert.assertEquals(false, con);
	}
	
	@Given("^then fill up the form for Commercial Shooting Permit leaving some fields blank$")
	public void then_fill_up_the_form_for_Commercial_Shooting_Permit_leaving_some_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.lastNameFld);
		re.getTextbox(Byloc.CustomerPortal.lastNameFld, "Last name", "Buenavista");
	    re.getTextbox(Byloc.CustomerPortal.middleNameFld, "Middle name", "Rivero");
	    re.getTextbox(Byloc.CustomerPortal.suffixFld, "Suffix", "Jr.");
	    re.scrollTo(Byloc.CustomerPortal.telephoneNumberfld, driver);
	    re.getTextbox(Byloc.CustomerPortal.telephoneNumberfld, "Telephone No.", "5459788");
	    re.getTextbox(Byloc.CustomerPortal.mobileNumberfld, "Mobile no.", "0977576"+Util.randomNum(9999, 1111));
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.emailfld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.designationfld, "Designaion", "Business Analyst");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.purposeParent, Byloc.childClassName);
	    re.click(Byloc.CustomerPortal.acomplishedShootFormChckbx, "Accomplsihed Details of Shoot Form");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.locationParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.shootingDateTimeFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.shootingHoursFld, "No.of Shooting Hours", ""+Util.randomNum(24, 3));
	    re.click(Byloc.CustomerPortal.withDroneChck, "With usage of drone");
	}

	@When("^the form for the Commercial Shooting Permit is submitted$")
	public void the_form_for_the_Commercial_Shooting_Permit_is_submitted() throws Throwable {
		the_form_for_the_Commercial_Shooting_Permit_will_be_submitted();
	}

	@Then("^the submission of application for commercial photography, videos and documentaries and shooting movies will not proceed$")
	public void the_submission_of_application_for_commercial_photography_videos_and_documentaries_and_shooting_movies_will_not_proceed() throws Throwable {
	    Thread.sleep(6000);
	    boolean con = driver.findElements(Byloc.invalidRequest).isEmpty();
	    Assert.assertEquals(false,con);
	}
	
	@Given("^then fill up the form for Commercial Shooting Permit with an email w/ invalid format$")
	public void then_fill_up_the_form_for_Commercial_Shooting_Permit_with_an_email_w_invalid_format() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.lastNameFld);
		re.getTextbox(Byloc.CustomerPortal.lastNameFld, "Last name", "Buenavista");
	    re.getTextbox(Byloc.CustomerPortal.firstNameFld, "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.middleNameFld, "Middle name", "Rivero");
	    re.getTextbox(Byloc.CustomerPortal.suffixFld, "Suffix", "Jr.");
	    re.getTextbox(Byloc.CustomerPortal.addressFld, "Address", "Mandaluyong,Metro Manila");
	    re.scrollTo(Byloc.CustomerPortal.telephoneNumberfld, driver);
	    re.getTextbox(Byloc.CustomerPortal.telephoneNumberfld, "Telephone No.", "5459788");
	    re.getTextbox(Byloc.CustomerPortal.mobileNumberfld, "Mobile no.", "0977576"+Util.randomNum(9999, 1111));
	    re.getTextbox(Byloc.CustomerPortal.companyfld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"emailcom";
	    re.getTextbox(Byloc.CustomerPortal.emailfld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.designationfld, "Designaion", "Business Analyst");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.purposeParent, Byloc.childClassName);
	    re.click(Byloc.CustomerPortal.acomplishedShootFormChckbx, "Accomplsihed Details of Shoot Form");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.locationParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.shootingDateTimeFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.shootingDateTimeFld, "Date and Time of Shooting", "05090020211200AM");
	    re.getTextbox(Byloc.CustomerPortal.shootingHoursFld, "No.of Shooting Hours", ""+Util.randomNum(24, 3));
	    re.click(Byloc.CustomerPortal.withDroneChck, "With usage of drone");
	}
	
	@And("^then fill up the form for Commercial Shooting Permit with a telephone no\\. w/ invalid format$")
	public void then_fill_up_the_form_for_Commercial_Shooting_Permit_with_a_telephone_no_w_invalid_format() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.lastNameFld);
		re.getTextbox(Byloc.CustomerPortal.lastNameFld, "Last name", "Buenavista");
	    re.getTextbox(Byloc.CustomerPortal.firstNameFld, "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.middleNameFld, "Middle name", "Rivero");
	    re.getTextbox(Byloc.CustomerPortal.suffixFld, "Suffix", "Jr.");
	    re.getTextbox(Byloc.CustomerPortal.addressFld, "Address", "Mandaluyong,Metro Manila");
	    re.scrollTo(Byloc.CustomerPortal.telephoneNumberfld, driver);
	    re.getTextbox(Byloc.CustomerPortal.telephoneNumberfld, "Telephone No.", "788");
	    re.getTextbox(Byloc.CustomerPortal.mobileNumberfld, "Mobile no.", "0977576"+Util.randomNum(9999, 1111));
	    re.getTextbox(Byloc.CustomerPortal.companyfld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.emailfld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.designationfld, "Designaion", "Business Analyst");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.purposeParent, Byloc.childClassName);
	    re.click(Byloc.CustomerPortal.acomplishedShootFormChckbx, "Accomplsihed Details of Shoot Form");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.locationParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.shootingDateTimeFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.shootingDateTimeFld, "Date and Time of Shooting", "05090020211200AM");
	    re.getTextbox(Byloc.CustomerPortal.shootingHoursFld, "No.of Shooting Hours", ""+Util.randomNum(24, 3));
	    re.click(Byloc.CustomerPortal.withDroneChck, "With usage of drone");
	}

	@And("^then fill up the form for Commercial Shooting Permit with a mobile no\\. w/ invalid format$")
	public void then_fill_up_the_form_for_Commercial_Shooting_Permit_with_a_mobile_no_w_invalid_format() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.lastNameFld);
		re.getTextbox(Byloc.CustomerPortal.lastNameFld, "Last name", "Buenavista");
	    re.getTextbox(Byloc.CustomerPortal.firstNameFld, "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.middleNameFld, "Middle name", "Rivero");
	    re.getTextbox(Byloc.CustomerPortal.suffixFld, "Suffix", "Jr.");
	    re.getTextbox(Byloc.CustomerPortal.addressFld, "Address", "Mandaluyong,Metro Manila");
	    re.scrollTo(Byloc.CustomerPortal.telephoneNumberfld, driver);
	    re.getTextbox(Byloc.CustomerPortal.telephoneNumberfld, "Telephone No.", "5459788");
	    re.getTextbox(Byloc.CustomerPortal.mobileNumberfld, "Mobile no.", "0947583645345343645725"+Util.randomNum(9999, 1111));
	    re.getTextbox(Byloc.CustomerPortal.companyfld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.emailfld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.designationfld, "Designaion", "Business Analyst");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.purposeParent, Byloc.childClassName);
	    re.click(Byloc.CustomerPortal.acomplishedShootFormChckbx, "Accomplsihed Details of Shoot Form");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.locationParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.shootingDateTimeFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.shootingDateTimeFld, "Date and Time of Shooting", "05090020211200AM");
	    re.getTextbox(Byloc.CustomerPortal.shootingHoursFld, "No.of Shooting Hours", ""+Util.randomNum(24, 3));
	    re.click(Byloc.CustomerPortal.withDroneChck, "With usage of drone");
	}
	
	@Then("^an error validation under email field will appear$")
	public void an_error_validation_under_email_field_will_appear() throws Throwable {
		Thread.sleep(3000);
		if(!driver.findElements(By.name("email")).isEmpty()) {
			re.scrollTo(By.name("email"), driver);
		}
	    boolean con = driver.findElements(Byloc.CustomerPortal.emailError).isEmpty();
	    Assert.assertEquals(false,con);
	}
	
	@And("^then click Apply Application button for Function Areas Permit$")
	public void then_click_Apply_Application_button_for_Function_Areas_Permit() throws Throwable {
	    re.click(Byloc.CustomerPortal.applyFunctionAreasBtn, "Apply Application");
	}

	@And("^then fill up the form for Function Areas Permit leaving some fields blank$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_leaving_some_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
	    re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "5456767");
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionPurposeParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionGroomFld, "Groom", "John Villanueva");
	    re.getTextbox(Byloc.CustomerPortal.functionBrideFld, "Bride", "Angela Montereal");
	    re.getTextbox(Byloc.CustomerPortal.functionOthersFld, "Others", "Mikael Cervantez");
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", ""+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}

	@When("^the form for the Function Areas Permit is submitted$")
	public void the_form_for_the_Function_Areas_Permit_is_submitted() throws Throwable {
		re.click(Byloc.CustomerPortal.functionSubmitBtn, "Submit");
	    re.click(Byloc.confirmSubmit, "Confirm submit");
	    Thread.sleep(7000);
	}

	@Then("^the submission of application for Function Areas Permit will not proceed$")
	public void the_submission_of_application_for_Function_Areas_Permit_will_not_proceed() throws Throwable {
		the_submission_of_application_for_commercial_photography_videos_and_documentaries_and_shooting_movies_will_not_proceed();
	}
	
	@And("^then fill up the form for Function Areas Permit w/ invalid telephone no\\. format$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_w_invalid_telephone_no_format() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.functionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "54dsfdg56767");
	    re.getTextbox(Byloc.CustomerPortal.functionMobileNoFld, "Mobile No.", "0912545"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionPurposeParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionGroomFld, "Groom", "John Villanueva");
	    re.getTextbox(Byloc.CustomerPortal.functionCelebrantFld, "Celebrant", "Katarina Borja");
	    re.getTextbox(Byloc.CustomerPortal.functionBrideFld, "Bride", "Angela Montereal");
	    re.getTextbox(Byloc.CustomerPortal.functionOthersFld, "Others", "Mikael Cervantez");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionVenueParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionTimeFld, "Time of Use", "1200AM");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", ""+Util.randomNum(99, 11));
	    re.getTextbox(Byloc.CustomerPortal.functionExtensionFld, "Extension", ""+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}
	
	@And("^then fill up the form for Function Areas Permit w/ invalid mobile no\\. format$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_w_invalid_mobile_no_format() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.functionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "5456767");
	    re.getTextbox(Byloc.CustomerPortal.functionMobileNoFld, "Mobile No.", "091254gdgsdfgdsfg5"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionPurposeParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionGroomFld, "Groom", "John Villanueva");
	    re.getTextbox(Byloc.CustomerPortal.functionCelebrantFld, "Celebrant", "Katarina Borja");
	    re.getTextbox(Byloc.CustomerPortal.functionBrideFld, "Bride", "Angela Montereal");
	    re.getTextbox(Byloc.CustomerPortal.functionOthersFld, "Others", "Mikael Cervantez");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionVenueParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionTimeFld, "Time of Use", "1200AM");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", ""+Util.randomNum(99, 11));
	    re.getTextbox(Byloc.CustomerPortal.functionExtensionFld, "Extension", ""+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}
	
	@And("^then fill up the form for Function Areas Permit w/ an invalid email format$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_w_an_invalid_email_format() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.functionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "5456767");
	    re.getTextbox(Byloc.CustomerPortal.functionMobileNoFld, "Mobile No.", "0912545"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"emailcom";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionPurposeParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionGroomFld, "Groom", "John Villanueva");
	    re.getTextbox(Byloc.CustomerPortal.functionCelebrantFld, "Celebrant", "Katarina Borja");
	    re.getTextbox(Byloc.CustomerPortal.functionBrideFld, "Bride", "Angela Montereal");
	    re.getTextbox(Byloc.CustomerPortal.functionOthersFld, "Others", "Mikael Cervantez");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionVenueParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionTimeFld, "Time of Use", "1200AM");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", ""+Util.randomNum(99, 11));
	    re.getTextbox(Byloc.CustomerPortal.functionExtensionFld, "Extension", ""+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}

	@When("^the submit button for Function Areas Permit is clicked$")
	public void the_submit_button_for_Function_Areas_Permit_is_clicked() throws Throwable {
		re.click(Byloc.CustomerPortal.functionSubmitBtn, "Submit");
	}
	
	@And("^then fill up the form for Function Areas Permit w/o selecting a purpose$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_w_o_selecting_a_purpose() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.functionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "5456767");
	    re.getTextbox(Byloc.CustomerPortal.functionMobileNoFld, "Mobile No.", "0912545"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionGroomFld, "Groom", "John Villanueva");
	    re.getTextbox(Byloc.CustomerPortal.functionCelebrantFld, "Celebrant", "Katarina Borja");
	    re.getTextbox(Byloc.CustomerPortal.functionBrideFld, "Bride", "Angela Montereal");
	    re.getTextbox(Byloc.CustomerPortal.functionOthersFld, "Others", "Mikael Cervantez");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionVenueParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionTimeFld, "Time of Use", "1200AM");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", ""+Util.randomNum(99, 11));
	    re.getTextbox(Byloc.CustomerPortal.functionExtensionFld, "Extension", ""+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}

	@And("^then fill up the form for Function Areas Permit w/o providing the name/s of the event owner/s$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_w_o_providing_the_name_s_of_the_event_owner_s() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.functionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "5456767");
	    re.getTextbox(Byloc.CustomerPortal.functionMobileNoFld, "Mobile No.", "0912545"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionPurposeParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionTimeFld, "Time of Use", "1200AM");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", ""+Util.randomNum(99, 11));
	    re.getTextbox(Byloc.CustomerPortal.functionExtensionFld, "Extension", ""+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}
	
	@And("^then fill up the form for Function Areas Permit w/o selecting a venue$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_w_o_selecting_a_venue() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.functionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "5456767");
	    re.getTextbox(Byloc.CustomerPortal.functionMobileNoFld, "Mobile No.", "0912545"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionPurposeParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionGroomFld, "Groom", "John Villanueva");
	    re.getTextbox(Byloc.CustomerPortal.functionCelebrantFld, "Celebrant", "Katarina Borja");
	    re.getTextbox(Byloc.CustomerPortal.functionBrideFld, "Bride", "Angela Montereal");
	    re.getTextbox(Byloc.CustomerPortal.functionOthersFld, "Others", "Mikael Cervantez");
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionTimeFld, "Time of Use", "1200AM");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", ""+Util.randomNum(99, 11));
	    re.getTextbox(Byloc.CustomerPortal.functionExtensionFld, "Extension", ""+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}

	@And("^then fill up the form for Function Areas Permit w/ a text input for the no\\. of guest field$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_w_a_text_input_for_the_no_of_guest_field() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.functionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "5456767");
	    re.getTextbox(Byloc.CustomerPortal.functionMobileNoFld, "Mobile No.", "0912545"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionPurposeParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionGroomFld, "Groom", "John Villanueva");
	    re.getTextbox(Byloc.CustomerPortal.functionCelebrantFld, "Celebrant", "Katarina Borja");
	    re.getTextbox(Byloc.CustomerPortal.functionBrideFld, "Bride", "Angela Montereal");
	    re.getTextbox(Byloc.CustomerPortal.functionOthersFld, "Others", "Mikael Cervantez");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionVenueParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionTimeFld, "Time of Use", "1200AM");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", "dsf"+Util.randomNum(99, 11));
	    re.getTextbox(Byloc.CustomerPortal.functionExtensionFld, "Extension", ""+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}

	@And("^then fill up the form for Function Areas Permit w/ a text input for the extension field$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_w_a_text_input_for_the_extension_field() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.functionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "5456767");
	    re.getTextbox(Byloc.CustomerPortal.functionMobileNoFld, "Mobile No.", "0912545"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionPurposeParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionGroomFld, "Groom", "John Villanueva");
	    re.getTextbox(Byloc.CustomerPortal.functionCelebrantFld, "Celebrant", "Katarina Borja");
	    re.getTextbox(Byloc.CustomerPortal.functionBrideFld, "Bride", "Angela Montereal");
	    re.getTextbox(Byloc.CustomerPortal.functionOthersFld, "Others", "Mikael Cervantez");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionVenueParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionTimeFld, "Time of Use", "1200AM");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", ""+Util.randomNum(99, 11));
	    re.getTextbox(Byloc.CustomerPortal.functionExtensionFld, "Extension", "sds"+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}

	@And("^then fill up the form for Function Areas Permit w/ valid and complete details$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_w_valid_and_complete_details() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.functionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "5456767");
	    re.getTextbox(Byloc.CustomerPortal.functionMobileNoFld, "Mobile No.", "0912545"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionPurposeParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionGroomFld, "Groom", "John Villanueva");
	    re.getTextbox(Byloc.CustomerPortal.functionCelebrantFld, "Celebrant", "Katarina Borja");
	    re.getTextbox(Byloc.CustomerPortal.functionBrideFld, "Bride", "Angela Montereal");
	    re.getTextbox(Byloc.CustomerPortal.functionOthersFld, "Others", "Mikael Cervantez");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionVenueParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionTimeFld, "Time of Use", "1200AM");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", ""+Util.randomNum(99, 11));
	    re.getTextbox(Byloc.CustomerPortal.functionExtensionFld, "Extension", ""+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}

	@And("^the form for the Function Areas Permit will be submitted$")
	public void the_form_for_the_Function_Areas_Permit_will_be_submitted() throws Throwable {
		the_form_for_the_Function_Areas_Permit_is_submitted();
	}

	@Then("^the submitted application for function areas permit should be present$")
	public void the_submitted_application_for_function_areaspermit_should_be_present() throws Throwable {
		the_submitted_application_for_commercial_photography_videos_and_documentaries_and_shooting_movies_should_be_present();
	}
	
	@And("^then fill up the form for Commercial Shooting Permit with a mobile no\\. w/ blank fields$")
	public void then_fill_up_the_form_for_Commercial_Shooting_Permit_with_a_mobile_no_w_blank_fields() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.submitCommercialBtn);
		re.scrollTo(Byloc.CustomerPortal.submitCommercialBtn, driver);
	}

	@And("^then fill up the form for Commercial Shooting Permit with a telephone no\\. w/ non-numerical characters$")
	public void then_fill_up_the_form_for_Commercial_Shooting_Permit_with_a_telephone_no_w_non_numerical_characters() throws Throwable {
		re.getTextbox(Byloc.CustomerPortal.lastNameFld, "Last name", "Buenavista");
	    re.getTextbox(Byloc.CustomerPortal.firstNameFld, "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.middleNameFld, "Middle name", "Rivero");
	    re.getTextbox(Byloc.CustomerPortal.suffixFld, "Suffix", "Jr.");
	    re.getTextbox(Byloc.CustomerPortal.addressFld, "Address", "Mandaluyong,Metro Manila");
	    re.scrollTo(Byloc.CustomerPortal.telephoneNumberfld, driver);
	    re.getTextbox(Byloc.CustomerPortal.telephoneNumberfld, "Telephone No.", "54dffsd59788");
	    re.getTextbox(Byloc.CustomerPortal.mobileNumberfld, "Mobile no.", "0977576"+Util.randomNum(9999, 1111));
	    re.getTextbox(Byloc.CustomerPortal.companyfld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.emailfld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.designationfld, "Designaion", "Business Analyst");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.purposeParent, Byloc.childClassName);
	    re.click(Byloc.CustomerPortal.acomplishedShootFormChckbx, "Accomplsihed Details of Shoot Form");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.locationParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.shootingDateTimeFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.shootingDateTimeFld, "Date and Time of Shooting", "05090020211200AM");
	    re.getTextbox(Byloc.CustomerPortal.shootingHoursFld, "No.of Shooting Hours", ""+Util.randomNum(24, 3));
	    re.click(Byloc.CustomerPortal.withDroneChck, "With usage of drone");
	}

	@And("^then fill up the form for Commercial Shooting Permit w/o middle name and suffix$")
	public void then_fill_up_the_form_for_Commercial_Shooting_Permit_w_o_middle_name_and_suffix() throws Throwable {
		re.getTextbox(Byloc.CustomerPortal.lastNameFld, "Last name", "Buenavista");
	    re.getTextbox(Byloc.CustomerPortal.firstNameFld, "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.addressFld, "Address", "Mandaluyong,Metro Manila");
	    re.scrollTo(Byloc.CustomerPortal.telephoneNumberfld, driver);
	    re.getTextbox(Byloc.CustomerPortal.telephoneNumberfld, "Telephone No.", "5459788");
	    re.getTextbox(Byloc.CustomerPortal.mobileNumberfld, "Mobile no.", "0977576"+Util.randomNum(9999, 1111));
	    re.getTextbox(Byloc.CustomerPortal.companyfld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.emailfld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.designationfld, "Designaion", "Business Analyst");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.purposeParent, Byloc.childClassName);
	    re.click(Byloc.CustomerPortal.acomplishedShootFormChckbx, "Accomplsihed Details of Shoot Form");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.locationParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.shootingDateTimeFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.shootingDateTimeFld, "Date and Time of Shooting", "05090020211200AM");
	    re.getTextbox(Byloc.CustomerPortal.shootingHoursFld, "No.of Shooting Hours", ""+Util.randomNum(24, 3));
	    re.click(Byloc.CustomerPortal.withDroneChck, "With usage of drone");
	}

	@And("^then fill up the form for Commercial Shooting Permit with a mobile no\\. w/ non-numerical characters$")
	public void then_fill_up_the_form_for_Commercial_Shooting_Permit_with_a_mobile_no_w_non_numerical_characters() throws Throwable {
		re.getTextbox(Byloc.CustomerPortal.lastNameFld, "Last name", "Buenavista");
	    re.getTextbox(Byloc.CustomerPortal.firstNameFld, "First name", "John");
	    re.getTextbox(Byloc.CustomerPortal.middleNameFld, "Middle name", "Rivero");
	    re.getTextbox(Byloc.CustomerPortal.suffixFld, "Suffix", "Jr.");
	    re.getTextbox(Byloc.CustomerPortal.addressFld, "Address", "Mandaluyong,Metro Manila");
	    re.scrollTo(Byloc.CustomerPortal.telephoneNumberfld, driver);
	    re.getTextbox(Byloc.CustomerPortal.telephoneNumberfld, "Telephone No.", "5459788");
	    re.getTextbox(Byloc.CustomerPortal.mobileNumberfld, "Mobile no.", "09775asas76"+Util.randomNum(9999, 1111));
	    re.getTextbox(Byloc.CustomerPortal.companyfld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.emailfld, "Email", email);
	    re.getTextbox(Byloc.CustomerPortal.designationfld, "Designaion", "Business Analyst");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.purposeParent, Byloc.childClassName);
	    re.click(Byloc.CustomerPortal.acomplishedShootFormChckbx, "Accomplsihed Details of Shoot Form");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.locationParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.shootingDateTimeFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.shootingDateTimeFld, "Date and Time of Shooting", "05090020211200AM");
	    re.getTextbox(Byloc.CustomerPortal.shootingHoursFld, "No.of Shooting Hours", ""+Util.randomNum(24, 3));
	    re.click(Byloc.CustomerPortal.withDroneChck, "With usage of drone");
	}
	
	@And("^then fill up the form for Function Areas Permit leaving all fields blank$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_leaving_all_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionSubmitBtn);
		re.scrollTo(Byloc.CustomerPortal.functionSubmitBtn, driver);
	}

	@And("^then fill up the form for Function Areas Permit w/ a mobile number w/ invalid length$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_w_a_mobile_number_w_invalid_format() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.functionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "5456767");
	    re.getTextbox(Byloc.CustomerPortal.functionMobileNoFld, "Mobile No.", "0912599945"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionPurposeParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionGroomFld, "Groom", "John Villanueva");
	    re.getTextbox(Byloc.CustomerPortal.functionCelebrantFld, "Celebrant", "Katarina Borja");
	    re.getTextbox(Byloc.CustomerPortal.functionBrideFld, "Bride", "Angela Montereal");
	    re.getTextbox(Byloc.CustomerPortal.functionOthersFld, "Others", "Mikael Cervantez");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionVenueParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionTimeFld, "Time of Use", "1200AM");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", ""+Util.randomNum(99, 11));
	    re.getTextbox(Byloc.CustomerPortal.functionExtensionFld, "Extension", ""+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}

	@And("^then fill up the form for Function Areas Permit w/ a telephone no\\. w/ invalid length$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_w_a_telephone_no_w_invalid_length() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionMiddleNameFld, "Middle Name", "Agrimano");
	    re.getTextbox(Byloc.CustomerPortal.functionSuffixFld, "Suffix", "N/A");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "545gafaf6767");
	    re.getTextbox(Byloc.CustomerPortal.functionMobileNoFld, "Mobile No.", "0912545"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionPurposeParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionGroomFld, "Groom", "John Villanueva");
	    re.getTextbox(Byloc.CustomerPortal.functionCelebrantFld, "Celebrant", "Katarina Borja");
	    re.getTextbox(Byloc.CustomerPortal.functionBrideFld, "Bride", "Angela Montereal");
	    re.getTextbox(Byloc.CustomerPortal.functionOthersFld, "Others", "Mikael Cervantez");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionVenueParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionTimeFld, "Time of Use", "1200AM");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", ""+Util.randomNum(99, 11));
	    re.getTextbox(Byloc.CustomerPortal.functionExtensionFld, "Extension", ""+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}

	@And("^then fill up the form for Function Areas Permit leaving the middle name and suffix fields blank$")
	public void then_fill_up_the_form_for_Function_Areas_Permit_leaving_the_middle_name_and_suffix_fields_blank() throws Throwable {
		el.waitVisible(Byloc.CustomerPortal.functionLastNameFld);
		re.getTextbox(Byloc.CustomerPortal.functionLastNameFld, "Last Name", "Guerra");
	    re.getTextbox(Byloc.CustomerPortal.functionFirstNameFld, "First Name", "Karl Andrey");
	    re.getTextbox(Byloc.CustomerPortal.functionAddressFld, "Address", "Mandaluyong, Metro Manila");
	    re.getTextbox(Byloc.CustomerPortal.functionTelephoneNoFld, "Telephone No.", "5456767");
	    re.getTextbox(Byloc.CustomerPortal.functionMobileNoFld, "Mobile No.", "0912545"+Util.randomNum(9999, 1111));
	    re.scrollTo(Byloc.CustomerPortal.functionCompanyFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionCompanyFld, "Company", "TLC");
	    email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
	    re.getTextbox(Byloc.CustomerPortal.functionEmailFld, "Email", email);
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionPurposeParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionGroomFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionGroomFld, "Groom", "John Villanueva");
	    re.getTextbox(Byloc.CustomerPortal.functionCelebrantFld, "Celebrant", "Katarina Borja");
	    re.getTextbox(Byloc.CustomerPortal.functionBrideFld, "Bride", "Angela Montereal");
	    re.getTextbox(Byloc.CustomerPortal.functionOthersFld, "Others", "Mikael Cervantez");
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionVenueParent, Byloc.childClassName);
	    re.scrollTo(Byloc.CustomerPortal.functionDateFld, driver);
	    re.getTextbox(Byloc.CustomerPortal.functionDateFld, "Date of Use", "11232021");
	    re.getTextbox(Byloc.CustomerPortal.functionTimeFld, "Time of Use", "1200AM");
	    re.getTextbox(Byloc.CustomerPortal.functionGuestFld, "No. of Guest", ""+Util.randomNum(99, 11));
	    re.getTextbox(Byloc.CustomerPortal.functionExtensionFld, "Extension", ""+Util.randomNum(99, 11));
	    el.clickOnRandomCheckbox(Byloc.CustomerPortal.functionOthersParent, Byloc.childClassName);
	}
	
	@And("^click Apply Application button for permit for non-commercial video and photography$")
	public void click_Apply_Application_button_for_permit_for_non_commercial_video_and_photography() throws Throwable {
	    el.waitVisible(Byloc.CustomerPortal.applyNonCommercialPermitBtn);
	    re.click(Byloc.CustomerPortal.applyNonCommercialPermitBtn, "Apply Application");
	}

	@And("^the user will leave all the fields for permit for non-commercial video and photography blank$")
	public void the_user_will_leave_all_the_fields_for_permit_for_non_commercial_video_and_photography_blank() throws Throwable {
		el.waitVisible(Byloc.NonCommercial.lastNameFld);
	    re.scrollTo(Byloc.NonCommercial.submitBtn, driver);
	}

	@When("^the blank form for permit for non-commercial video and photography is submitted$")
	public void the_blank_form_for_permit_for_non_commercial_video_and_photography_is_submitted() throws Throwable {
	    re.click(Byloc.NonCommercial.submitBtn, "Submit");
	}

	@Then("^required validation messages will appear$")
	public void required_validation_messages_will_appear() throws Throwable {
		Thread.sleep(2000);
	    boolean con = driver.findElements(Byloc.requiredLbl).isEmpty();
	    Assert.assertEquals(false, con);
	}
	
	@And("^the user will fill up the form for permit for non-commercial video and photography leaving the middle name and suffix fields empty$")
	public void the_user_will_fill_up_the_form_for_permit_for_non_commercial_video_and_photography_leaving_the_middle_name_and_suffix_fields_empty() throws Throwable {
		el.waitVisible(Byloc.NonCommercial.lastNameFld);
		re.getTextbox(Byloc.NonCommercial.lastNameFld, "Last name", "Villaraza");
		re.getTextbox(Byloc.NonCommercial.firstNameFld, "First name", "Desir");
		re.getTextbox(Byloc.NonCommercial.addressFld, "Address", "Mandaluyong, Metro Manila");
		re.scrollTo(Byloc.NonCommercial.telFld, driver);
		re.getTextbox(Byloc.NonCommercial.telFld, "Telephone no.", "5458799");
		re.getTextbox(Byloc.NonCommercial.mobileFld, "Mobile no.", "988777777");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.NonCommercial.emailFld, "Email", email);
		el.clickOnRandomCheckbox(Byloc.NonCommercial.purposePrnt, Byloc.childRadioClassName);
		el.clickOnRandomCheckbox(Byloc.NonCommercial.locationPrnt, Byloc.childClassName);
		re.scrollTo(Byloc.NonCommercial.dateFld, driver);
		re.getTextbox(Byloc.NonCommercial.dateFld, "Date", "03272021");
		el.clickOnRandomCheckbox(Byloc.NonCommercial.detailsPrnt, Byloc.childClassName);
		re.getTextbox(Byloc.NonCommercial.adultFld, "Adult", "7");
		re.getTextbox(Byloc.NonCommercial.childrenFld, "Adult", "2");
	}

	@And("^the user will submit the form permit for non-commercial video and photography$")
	public void the_user_will_submit_the_form_permit_for_non_commercial_video_and_photography() throws Throwable {
		re.click(Byloc.NonCommercial.submitBtn, "Submit");
	    re.click(Byloc.confirmSubmit, "Confirm submit");
	    Thread.sleep(7000);
	}
	
	@And("^the user will fill up the form for permit for non-commercial video and photography and provide an invalid value for suffix field instead of Jr\\., Sr\\., I, etc\\.$")
	public void the_user_will_fill_up_the_form_for_permit_for_non_commercial_video_and_photography_and_provide_an_invalid_value_for_suffix_field_instead_of_Jr_Sr_I_etc() throws Throwable {
		el.waitVisible(Byloc.NonCommercial.lastNameFld);
		re.getTextbox(Byloc.NonCommercial.lastNameFld, "Last name", "Villaraza");
		re.getTextbox(Byloc.NonCommercial.firstNameFld, "First name", "Desir");
		re.getTextbox(Byloc.NonCommercial.middleNameFld, "Middle name", "Mendez");
		re.getTextbox(Byloc.NonCommercial.suffixFld, "Suffix", "as");
		re.getTextbox(Byloc.NonCommercial.addressFld, "Address", "Mandaluyong, Metro Manila");
		re.scrollTo(Byloc.NonCommercial.telFld, driver);
		re.getTextbox(Byloc.NonCommercial.telFld, "Telephone no.", "5458799");
		re.getTextbox(Byloc.NonCommercial.mobileFld, "Mobile no.", "988777777");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.NonCommercial.emailFld, "Email", email);
		el.clickOnRandomCheckbox(Byloc.NonCommercial.purposePrnt, Byloc.childRadioClassName);
		el.clickOnRandomCheckbox(Byloc.NonCommercial.locationPrnt, Byloc.childClassName);
		re.scrollTo(Byloc.NonCommercial.dateFld, driver);
		re.getTextbox(Byloc.NonCommercial.dateFld, "Date", "03272021");
		el.clickOnRandomCheckbox(Byloc.NonCommercial.detailsPrnt, Byloc.childClassName);
		re.getTextbox(Byloc.NonCommercial.adultFld, "Adult", "7");
		re.getTextbox(Byloc.NonCommercial.childrenFld, "Adult", "2");
	}

	@When("^the submit button for non-commercial video and photography is clicked$")
	public void the_submit_button_for_non_commercial_video_and_photography_is_clicked() throws Throwable {
		re.click(Byloc.NonCommercial.submitBtn, "Submit");
	}

	@Then("^a validation message for invalid format will appear under the suffix field$")
	public void a_validation_message_for_invalid_format_will_appear_under_the_suffix_field() throws Throwable {
	    re.scrollTo(Byloc.NonCommercial.suffixFld, driver);
	    boolean con = driver.findElements(Byloc.invalidSuffix).isEmpty();
	    Assert.assertEquals(false, con);
	}
	
	@And("^the user will fill up the form for permit for non-commercial video and photography and provide an valid value for suffix field$")
	public void the_user_will_fill_up_the_form_for_permit_for_non_commercial_video_and_photography_and_provide_an_valid_value_for_suffix_field() throws Throwable {
		el.waitVisible(Byloc.NonCommercial.lastNameFld);
		re.getTextbox(Byloc.NonCommercial.lastNameFld, "Last name", "Villaraza");
		re.getTextbox(Byloc.NonCommercial.firstNameFld, "First name", "Desir");
		re.getTextbox(Byloc.NonCommercial.suffixFld, "Suffix", "II");
		re.getTextbox(Byloc.NonCommercial.addressFld, "Address", "Mandaluyong, Metro Manila");
		re.scrollTo(Byloc.NonCommercial.telFld, driver);
		re.getTextbox(Byloc.NonCommercial.telFld, "Telephone no.", "5458799");
		re.getTextbox(Byloc.NonCommercial.mobileFld, "Mobile no.", "988777777");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.NonCommercial.emailFld, "Email", email);
		el.clickOnRandomCheckbox(Byloc.NonCommercial.purposePrnt, Byloc.childRadioClassName);
		el.clickOnRandomCheckbox(Byloc.NonCommercial.locationPrnt, Byloc.childClassName);
		re.scrollTo(Byloc.NonCommercial.dateFld, driver);
		re.getTextbox(Byloc.NonCommercial.dateFld, "Date", "03272021");
		el.clickOnRandomCheckbox(Byloc.NonCommercial.detailsPrnt, Byloc.childClassName);
		re.getTextbox(Byloc.NonCommercial.adultFld, "Adult", "7");
		re.getTextbox(Byloc.NonCommercial.childrenFld, "Adult", "2");
	}

	@Then("^the confirm submit button will appear$")
	public void the_confirm_submit_button_will_appear() throws Throwable {
		Thread.sleep(2000);
		boolean con = driver.findElements(Byloc.confirmSubmit).isEmpty();
	    Assert.assertEquals(false, con);
	}
	
	@And("^the user will fill up the form for permit for non-commercial video and photography and provide a telephone no\\. w/ invalid format$")
	public void the_user_will_fill_up_the_form_for_permit_for_non_commercial_video_and_photography_and_provide_a_telephone_no_w_invalid_format() throws Throwable {
		el.waitVisible(Byloc.NonCommercial.lastNameFld);
		re.getTextbox(Byloc.NonCommercial.lastNameFld, "Last name", "Villaraza");
		re.getTextbox(Byloc.NonCommercial.firstNameFld, "First name", "Desir");
		re.getTextbox(Byloc.NonCommercial.middleNameFld, "Middle name", "Mendez");
		re.getTextbox(Byloc.NonCommercial.suffixFld, "Suffix", "II");
		re.getTextbox(Byloc.NonCommercial.addressFld, "Address", "Mandaluyong, Metro Manila");
		re.scrollTo(Byloc.NonCommercial.telFld, driver);
		re.getTextbox(Byloc.NonCommercial.telFld, "Telephone no.", "54599");
		re.getTextbox(Byloc.NonCommercial.mobileFld, "Mobile no.", "988777777");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.NonCommercial.emailFld, "Email", email);
		el.clickOnRandomCheckbox(Byloc.NonCommercial.purposePrnt, Byloc.childRadioClassName);
		el.clickOnRandomCheckbox(Byloc.NonCommercial.locationPrnt, Byloc.childClassName);
		re.scrollTo(Byloc.NonCommercial.dateFld, driver);
		re.getTextbox(Byloc.NonCommercial.dateFld, "Date", "03272021");
		el.clickOnRandomCheckbox(Byloc.NonCommercial.detailsPrnt, Byloc.childClassName);
		re.getTextbox(Byloc.NonCommercial.adultFld, "Adult", "7");
		re.getTextbox(Byloc.NonCommercial.childrenFld, "Adult", "2");
	}

	@Then("^a validation message for invalid format will appear under the telephone no\\. field$")
	public void a_validation_message_for_invalid_format_will_appear_under_the_telephone_no_field() throws Throwable {
	    re.scrollTo(Byloc.NonCommercial.telFld, driver);
	    boolean con = driver.findElements(Byloc.invalidFormatTelephoneError).isEmpty();
	    Assert.assertEquals(false, con);
	}
	
	@When("^the user will fills up the form for permit for non-commercial video and photography and provide a telephone no\\. w/ non-numercial characters$")
	public void the_user_will_fills_up_the_form_for_permit_for_non_commercial_video_and_photography_and_provide_a_telephone_no_w_non_numercial_characters() throws Throwable {
		el.waitVisible(Byloc.NonCommercial.lastNameFld);
		re.getTextbox(Byloc.NonCommercial.lastNameFld, "Last name", "Villaraza");
		re.getTextbox(Byloc.NonCommercial.firstNameFld, "First name", "Desir");
		re.getTextbox(Byloc.NonCommercial.middleNameFld, "Middle name", "Mendez");
		re.getTextbox(Byloc.NonCommercial.suffixFld, "Suffix", "II");
		re.getTextbox(Byloc.NonCommercial.addressFld, "Address", "Mandaluyong, Metro Manila");
		re.scrollTo(Byloc.NonCommercial.telFld, driver);
		re.getTextbox(Byloc.NonCommercial.telFld, "Telephone no.", tel+"b");
		re.getTextbox(Byloc.NonCommercial.mobileFld, "Mobile no.", "988777777");
		email = "AutomatedEmail"+Util.generateRandomNumber(999, 111)+"@email.com";
		re.getTextbox(Byloc.NonCommercial.emailFld, "Email", email);
		el.clickOnRandomCheckbox(Byloc.NonCommercial.purposePrnt, Byloc.childRadioClassName);
		el.clickOnRandomCheckbox(Byloc.NonCommercial.locationPrnt, Byloc.childClassName);
		re.scrollTo(Byloc.NonCommercial.dateFld, driver);
		re.getTextbox(Byloc.NonCommercial.dateFld, "Date", "03272021");
		el.clickOnRandomCheckbox(Byloc.NonCommercial.detailsPrnt, Byloc.childClassName);
		re.getTextbox(Byloc.NonCommercial.adultFld, "Adult", "7");
		re.getTextbox(Byloc.NonCommercial.childrenFld, "Adult", "2");
	}

	@Then("^the non-numerical character will not be accepted in the telephone no\\.field$")
	public void the_non_numerical_character_will_not_be_accepted_in_the_telephone_no_field() throws Throwable {
		re.scrollTo(Byloc.NonCommercial.addressFld, driver);
		Thread.sleep(2000);
		String val = re.getText(Byloc.NonCommercial.telFld, "Accepeted Tel #");
		Util.print(val +" -> "+tel);
	    boolean con = val.equalsIgnoreCase(tel);
	    Assert.assertEquals(true, con);
	}
}
