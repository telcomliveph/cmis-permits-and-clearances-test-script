package com.tlc.DOT_CMIS;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class SharedClass {
	
	WebDriver driver;
	String url;
	
	@Before
	public WebDriver setup() {
		final String dir = System.getProperty("user.dir");
        String path = dir+"\\resources\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver",path);
        
        if(driver == null) {
        	driver = new ChromeDriver();
        }
        
		this.driver = driver;
		return driver;
	}
}
