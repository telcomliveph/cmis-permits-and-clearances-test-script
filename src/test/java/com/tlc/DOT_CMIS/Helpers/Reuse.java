package com.tlc.DOT_CMIS.Helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.selenium.helper.Elements;
import com.selenium.helper.Util;

public class Reuse {

	private WebDriver driver;
	private static Elements el;
	private static Util ut = new Util();
	
	public Reuse(WebDriver driver) {
		
		el = new Elements(driver, 10);
		
	}

	public static WebElement waits(By by) {
		
		return el.waitClickable(el.waitPresence(by));
	}

	public static String getText(By by, String desc) {
		String stat = null;
		try {
			Thread.sleep(1000);
			stat = el.getText(by);
			ut.print(stat);
			
		}
		catch(Exception e) {
			e.printStackTrace();
			ut.print(desc + " is not found!");
			getText(by, desc);
		}
		return stat;
	}
	
	public static void getTextbox(By by , String desc , String input) {
		
		try {
			Thread.sleep(1000);
				Util.inputData(waits(by), desc, input);
				Util.print(desc+" input: "+input);
		}
		catch(Exception e) {
			e.printStackTrace();
			Util.print("Cant find "+ desc + " text field!");
			getTextbox(by , desc , input);
		}
	}
	
	public static void click(By by, String buttonName) {
		try {
			Thread.sleep(1000);
			waits(by).click();
			Util.print(buttonName + " button is clicked!");
		}
		
		catch(Exception e) {
			Util.print(buttonName + " button is not clicked!");
			click(by, buttonName);
		}
	}
	
	// use "sel" for random selection, and "selSpec" for specific selection
	public static void sel (By drpd, String desc) {
		try {
			Thread.sleep(2000);
			waits(drpd).click();

			WebElement dropdown =  waits(drpd);
		    List<WebElement> options = dropdown.findElements(By.tagName("option"));
		    Thread.sleep(1000);
		    
		    int count = options.size()-1;
		    int d = (int)(Math.random() * count); 
		    Thread.sleep(1000);
		    options.get(d+1).click();
		    
		    Util.print(desc + " has been chosen!");
		}
		catch(Exception e) {
			Util.print(desc + " has not been chosen!");
			sel(drpd, desc);
		}
	}
	
	public static void selSpec (By drpd, String desc, String input) {
		try {
			Thread.sleep(2000);
			waits(drpd).click();

			WebElement dropdown =  waits(drpd);
		    List<WebElement> options = dropdown.findElements(By.tagName("option"));
		    Thread.sleep(1000);
		    
		    int count = options.size()-1;
		    int d = Integer.parseInt(input);
		    options.get(d-1).click();
		    
		    Util.print(desc + " has been chosen!");
		}
		catch(Exception e) {
			Util.print(desc + " has not been chosen!");
			sel(drpd, desc);
		}
	}
	
	public static void scrollTo(By by, WebDriver driver) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(by));
	}
	
	// Newly Added Functions 11/17/2020
	public static void uploadFile(By by, String path, WebDriver driver) {
		
		WebElement element = driver.findElement(by);
		final String facedir = System.getProperty("user.dir")+"\\";
        String faceImg = facedir+path;
		Util.print("Uploaded file path => "+faceImg);
        element.sendKeys(faceImg);
	}
	
	public static String readWebuserCreds(String filePath) {
	    String content = "";
		File myObj=new File(System.getProperty("user.dir")+"\\"+filePath);
		try {
	      Scanner myReader = new Scanner(myObj);  
	      while (myReader.hasNextLine()) {
	        String data = myReader.nextLine();
	        if(content.equals(null)) {
	        	content = content+"|"+data;
	        }
	        else {
	        	content = content+data;
	        }
	      }
	      myReader.close();
	    } catch (FileNotFoundException e) {
	      System.out.println("An error occurred.");
	      e.printStackTrace();
	    }
		return content;
	}
	
	public static Date getCurrentDate() {
		
//		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");  
        Date date = new Date();
//        String currDate = formatter.format(date);
//        System.out.println("Current Date: "+currDate);
        
        return date;
	}
	
	// Methods for clicking keyboard keys
	public static void clickEnter(By by,WebDriver driver) {
		try {
			Thread.sleep(2000);
			WebElement textbox = driver.findElement(by);
			textbox.sendKeys(Keys.ENTER);
			Util.print("Enter key is clicked");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void drawSignature(WebElement element, WebDriver driver) {
//		Actions builder = new Actions(driver);
//	    Action drawAction = builder.moveToElement(element,135,15) //start points x axis and y axis. 
//	              .click()
//	              .moveByOffset(200, 60) // 2nd points (x1,y1)
//	              .click()
//	              .moveByOffset(100, 70)// 3rd points (x2,y2)
//	              .doubleClick()
//	              .build();
//	    drawAction.perform();
		
//		Select toolDraw = new Select(element);
//		toolDraw.selectByValue("pencil");
		
		Actions builder = new Actions(driver);
		builder.clickAndHold(element).moveByOffset(10, 50).
		moveByOffset(50,10).
		moveByOffset(-10,-50).
		moveByOffset(-50,-10).release().perform();
	}
}
