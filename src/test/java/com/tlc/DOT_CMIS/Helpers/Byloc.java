package com.tlc.DOT_CMIS.Helpers;

import org.openqa.selenium.By;

public class Byloc {

/**
 * 
	New format for dynamic xpath: 
		//element[text()='elementText']
		//element[contains(text(), 'elementText')]
		 * 
	Note: Use this if element has no ID to be used
 *
**/
	
	public static By invalidRequest= By.xpath("//*[contains(text(), 'Invalid Request')]");
	public static final String childClassName = "custom-checkbox"; // class name for checkboxes
	public static final String childRadioClassName = "custom-radio"; // class name for radio buttons
	public static final By confirmSubmit = By.xpath("/html/body/div[2]/div[2]/div/div/div/div/div/div/div/div[4]/button[1]");
	public static final By requiredLbl = By.xpath("//label[contains(text(), 'This field is required.')]");
	public static final By invalidFormatTelephoneError = By.id("telephone-error");
	public static final By invalidFormatMobileError = By.id("mobile-error");
	public static final By invalidSuffix = By.xpath("//label[contains(text(), 'Invalid format')]");
	
	public static class CustomerPortal {
		
		// Menus
		public static final By homeLnk = By.xpath("//*[@id=\"sidenav-collapse-main\"]/ul/li[1]/a");
		public static final By applyUPCDDLnk = By.xpath("//*[@id=\"sidenav-collapse-main\"]/ul/li[2]/a");
		public static final By applyBMDLnk = By.xpath("//*[@id=\"sidenav-collapse-main\"]/ul/li[3]/a");
		
		public static final By nextTablePage = By.className("fa-angle-right");
		public static final By applyCommercialBtn = By.xpath("//*[@id=\"permitlist\"]/tbody/tr[2]/td[3]/button");
		public static final By applyFunctionAreasBtn = By.xpath("//*[@id=\"permitlist\"]/tbody/tr[1]/td[3]/button");
		
		public static final By applyLocationalBtn = By.xpath("//*[@id=\"permitlist\"]/tbody/tr[10]/td[3]/button");
		public static final By applyExcavationBtn = By.xpath("//*[@id=\"permitlist\"]/tbody/tr[7]/td[3]/button");
		public static final By applySidewalkEnclosureBtn = By.xpath("//*[@id=\"permitlist\"]/tbody/tr[1]/td[3]/button");
		public static final By applySidewalkConstructionBtn = By.xpath("//*[@id=\"permitlist\"]/tbody/tr[9]/td[3]/button");
		public static final By applyWiringClearanceBtn = By.xpath("//*[@id=\"permitlist\"]/tbody/tr[3]/td[3]/button");
		public static final By applyPostingOfStreamerBtn = By.xpath("//*[@id=\"permitlist\"]/tbody/tr[5]/td[3]/button");
		public static final By applyScaffoldingPermitBtn = By.xpath("//*[@id=\"permitlist\"]/tbody/tr[8]/td[3]/button");
		public static final By applyNonCommercialPermitBtn = By.xpath("//*[@id=\"permitlist\"]/tbody/tr[3]/td[3]/button");
		
		//form for commercial permit
		public static final By lastNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[2]/div/input");
		public static final By firstNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[3]/div/input");
		public static final By middleNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[4]/div/input");
		public static final By suffixFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[5]/div/input");
		public static final By addressFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[6]/div/input");
		public static final By telephoneNumberfld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[1]/div/input");
		public static final By mobileNumberfld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[2]/div/input");
		public static final By companyfld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[3]/div/input");
		public static final By emailfld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[4]/div/input");
		public static final By designationfld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[5]/div/input");
		public static final By purposeParent = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[6]/div");
		public static final By acomplishedShootFormChckbx = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[7]/div/div[1]");
		public static final By documentaryNarrativeChckbx = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[7]/div/div[2]");
		public static final By locationParent = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[8]/div");
		public static final By shootingDateTimeFld =By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[9]/div/input");
		public static final By shootingHoursFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[10]/div/input");
		public static final By withDroneChck = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[11]/div/div[1]");
		public static final By withFireworksChck = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[11]/div/div[2]");
		public static final By submitCommercialBtn = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[3]/button");
		//=======================================================================================================
		
		//form for locational clearance
		public static final By locationalLastNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[2]/div/input");
		public static final By locationalFirstNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[3]/div/input");
		public static final By locationalMiddleNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[4]/div/input");
		public static final By locationalSuffixFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[5]/div/input");
		public static final By locationalApplicationTypeParent = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[6]/div");
		public static final By locationalAddressFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[7]/div/input");
		public static final By locationalTelNoFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[1]/div/input");
		public static final By locationalMobileNoFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[2]/div/input");
		public static final By locationalEmailFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[3]/div/input");
		public static final By locationalBusinessNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[4]/div/input");
		public static final By locationalBusinessLocationFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[5]/div/input");
		public static final By locationalLineOfBusinessFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[6]/div/input");
		public static final By submitLocationalBtn = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[3]/button");
		public static final By emailError = By.id("email-error");
		//=======================================================================================================
		
		//form for excavation permit
		public static final By excavationCompanyNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[1]/div/input");
		public static final By excavationCompanyAddressFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[2]/div/input");
		public static final By excavationTelNoFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[1]/div/input");
		public static final By excavationMobileNoFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[2]/div/input");
		public static final By excavationEmailFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[3]/div/input");
		public static final By excavationPurposeFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[4]/div/input");
		public static final By excavationLocationFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[5]/div/input");
		public static final By excavationRepresentativeFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[6]/div/input");
		public static final By excavationLastNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[8]/div/input");
		public static final By excavationFirstNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[9]/div/input");
		public static final By excavationMiddleNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[10]/div/input");
		public static final By excavationSuffixFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[11]/div/input");
		public static final By excavationSubmit = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[3]/button");
		//=======================================================================================================
		
		//form for temporary sidewalk/street enclosure and occupancy permit
		public static final By sidewalkCompanyNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[1]/div/input");
		public static final By sidewalkAddressFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[2]/div/input");
		public static final By sidewalkTelNoFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[1]/div/input");
		public static final By sidewalkMobileNoFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[2]/div/input");
		public static final By sidewalkEmailFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[3]/div/input");
		public static final By sidewalkFrontageFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[5]/div/input");
		public static final By sidewalkWidthFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[6]/div/input");
		public static final By sidewalkSquareMeterFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[7]/div/input");
		public static final By sidewalkLocationFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[8]/div/input");
		public static final By sidewalkPeriodFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[9]/div/input");
		public static final By sidewalkRepresentativeFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[10]/div/input");
		public static final By sidewalkSubmitBtn = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[3]/button");
		//=======================================================================================================
		
		//form for sidewalk construction permit
		public static final By sidewalkConstructionLastNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[2]/div/input");
		public static final By sidewalkConstructionFirstNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[3]/div/input");
		public static final By sidewalkConstructionMiddleNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[4]/div/input");
		public static final By sidewalkConstructionSuffixFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[5]/div/input");
		public static final By sidewalkConstructionAddressFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[6]/div/input");
		public static final By sidewalkConstructionTelephoneNoFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[1]/div/input");
		public static final By sidewalkConstructionMobileNoFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[2]/div/input");
		public static final By sidewalkConstructionEmailFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[3]/div/input");
		public static final By sidewalkConstructionBuildingFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[4]/div/input");
		public static final By sidewalkConstructionLocationFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[5]/div/input");
		public static final By sidewalkConstructionRepresentativeFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[6]/div/input");
		public static final By sidewalkConstructionSubmitBtn= By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[3]/button");
		//=======================================================================================================
		
		//form for wiring clearance
		public static final By wiringLastNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[2]/div/input");
		public static final By wiringFirstNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[3]/div/input");
		public static final By wiringMiddleNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[4]/div/input");
		public static final By wiringSuffixFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[5]/div/input");
		public static final By wiringAddressFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[6]/div/input");
		public static final By wiringTelephoneNoFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[1]/div/input");
		public static final By wiringMobileNoFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[2]/div/input");
		public static final By wiringEmailFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[3]/div/input");
		public static final By wiringPurposeParent = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[4]/div");
		public static final By wiringLocationFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[5]/div/input");
		public static final By wiringFloorFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[6]/div/input");
		public static final By wiringSubmitBtn = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[4]/button");
		//=======================================================================================================
		
		//form for function areas permit
		public static final By functionLastNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[2]/div/input");
		public static final By functionFirstNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[3]/div/input");
		public static final By functionMiddleNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[4]/div/input");
		public static final By functionSuffixFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[5]/div/input");
		public static final By functionAddressFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[6]/div/input");
		public static final By functionTelephoneNoFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[1]/div/input");
		public static final By functionMobileNoFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[2]/div/input");
		public static final By functionCompanyFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[3]/div/input");
		public static final By functionEmailFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[4]/div/input");
		public static final By functionDesignationFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[5]/div/input");
		public static final By functionPurposeParent = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[6]/div");
		
		public static final By functionGroomFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[8]/div/input");
		public static final By functionBrideFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[10]/div/input");
		public static final By functionDebutantFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[12]/div/input");
		public static final By functionCelebrantFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[9]/div/input");
		public static final By functionOthersFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[11]/div/input");
		
		public static final By functionVenueParent = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[13]/div");
		
		public static final By functionDateFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[15]/div/input");
		public static final By functionTimeFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[16]/div/input");
		public static final By functionGuestFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[17]/div/input");
		public static final By functionExtensionFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[18]/div/input");
		
		public static final By functionOthersParent = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[19]/div");
		
		public static final By functionSubmitBtn = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[3]/button");
		//=======================================================================================================
		
		//form for posting streamer and tarpaulin permit
		public static final By tarpLastNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[2]/div/input");
		public static final By tarpFirstNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[3]/div/input");
		public static final By tarpMiddleNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[4]/div/input");
		public static final By tarpSuffixFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[5]/div/input");
		public static final By tarpAddressFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[6]/div/input");
		public static final By tarpTelephoneFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[1]/div/input");
		public static final By tarpMobileFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[2]/div/input");
		public static final By tarpEmailFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[3]/div/input");
		public static final By tarpAnnouncementFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[4]/div/input");
		public static final By tarpScheduleFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[5]/div/input");
		public static final By tarpSubmitBtn = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[3]/button");
		//=======================================================================================================
		
		//form for scaffolding permit
		public static final By scaffoldingLastNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[2]/div/input");
		public static final By scaffoldingFirstNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[3]/div/input");
		public static final By scaffoldingMiddleNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[4]/div/input");
		public static final By scaffoldingSuffixFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[5]/div/input");
		public static final By scaffoldingBuildingFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[1]/div/input");
		public static final By scaffoldingAddressFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[2]/div/input");
		public static final By scaffoldingTelephoneFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[3]/div[1]/div/input");
		public static final By scaffoldingMobileFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[3]/div[2]/div/input");
		public static final By scaffoldingEmailFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[3]/div[3]/div/input");
		public static final By scaffoldingLocationFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[3]/div[4]/div/input");
		public static final By scaffoldingFrontageFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[4]/div[2]/div/input");
		public static final By scaffoldingPeriodStartFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[4]/div[3]/div/div/div[1]/div/input");
		public static final By scaffoldingPeriodEndFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[4]/div[3]/div/div/div[2]/div/input");
		public static final By scaffoldingRepresentativeFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[5]/div/div/input");
		public static final By scaffoldingSubmitBtn = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[6]/button");
		//=======================================================================================================
	}
	
	public static class NonCommercial {
		public static final By invalidAmountMsg = By.xpath("//label[contains(text(), 'Invalid, amount must not less than 1')]");
		
		public static final By lastNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[2]/div/input");
		public static final By firstNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[3]/div/input");
		public static final By middleNameFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[4]/div/input");
		public static final By suffixFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[5]/div/input");
		public static final By addressFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[1]/div[6]/div/input");
		public static final By telFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[1]/div/input");
		public static final By mobileFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[2]/div/input");
		public static final By emailFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[3]/div/input");
		public static final By purposePrnt = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[4]/div");
		public static final By locationPrnt = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[5]/div");
		public static final By dateFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[7]/div/input");
		public static final By timePrnt = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[8]/div/div");
		public static final By detailsPrnt = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[9]/div");
		public static final By adultFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[11]/div/input");
		public static final By childrenFld = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[2]/div[12]/div/input");
		public static final By submitBtn = By.xpath("/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/form/div[3]/button");
	}
	
	public static class BMDPortal {
		public static final By permitsAndClearancesMenu = By.xpath("//a/span[contains(text(), 'Permits and Clearances ')]");
		public static final By pendingApplicationsMenu = By.xpath("//a/span[contains(text(), 'Pending Applications ')]");
		public static final By tableLastPageBtn = By.xpath("//*[@id=\"main-wrapper\"]/div/div[2]/div/div[1]/div/div/div[1]/div[2]/ul/li[5]/a");
		public static final By footer = By.xpath("//*[@id=\"main-wrapper\"]/div/footer");
	}
}
