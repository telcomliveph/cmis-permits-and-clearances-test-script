@NONCOMMERCIALPERMIT
Feature: Apply permit for Non-Commercial Video and Photography

#@NONCOMMERCIALPERMIT-001
#Scenario: Apply permit for non-commercial video and photography w/ blank fields
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will leave all the fields for permit for non-commercial video and photography blank
#	When the blank form for permit for non-commercial video and photography is submitted
#	Then required validation messages will appear
#
#@NONCOMMERCIALPERMIT-002
#Scenario: Apply permit for non-commercial video and photography while leaving the middle name and suffix fields empty
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography leaving the middle name and suffix fields empty
#	And the user will submit the form permit for non-commercial video and photography
#	And the user will go to the dashboard page to check submitted UPCDD application
#	And click the Permits an Clearances menu to check submitted UPCDD application
#	And then click the Pending Applications menu to check submitted UPCDD application
#	When the user goes to the last part of the table to look for the to check submitted UPCDD application
#	Then the submitted UPCDD application should be present
#@NONCOMMERCIALPERMIT-003	
#Scenario: Apply permit for non-commercial video and photography while providing a suffix w/ invalid format
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography and provide an invalid value for suffix field instead of Jr., Sr., I, etc.
#	When the submit button for non-commercial video and photography is clicked
#	Then a validation message for invalid format will appear under the suffix field
#	
#@NONCOMMERCIALPERMIT-004
#Scenario: Apply permit for non-commercial video and photography while providing a suffix w/ valid format
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography and provide an valid value for suffix field
#	When the submit button for non-commercial video and photography is clicked
#	Then the confirm submit button will appear
#	
#@NONCOMMERCIALPERMIT-005
#Scenario: Apply permit for non-commercial video and photography while providing a telephone no. w/ invalid format
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography and provide a telephone no. w/ invalid format
#	When the submit button for non-commercial video and photography is clicked
#	Then a validation message for invalid format will appear under the telephone no. field
#
#@NONCOMMERCIALPERMIT-006	#re.getText is not working on "Then"
#Scenario: Apply permit for non-commercial video and photography while providing a telephone no. w/ non-numerical characters
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	When the user will fills up the form for permit for non-commercial video and photography and provide a telephone no. w/ non-numercial characters
#	Then the non-numerical character will not be accepted in the telephone no.field
#	
Scenario: Apply permit for non-commercial video and photography while leaving the mobile no. field blank
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
	And the user will fill up the form for permit for non-commercial video and photography and leave the mobile no. field blank
#	When the submit button for non-commercial video and photography is clicked
	Then a required validation message will appear under the mobile no. field
#	
#Scenario: Apply permit for non-commercial video and photography while providing a mobile no. w/ invalid format
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography and provide a mobile no. w/ invalid format
#	When the submit button for non-commercial video and photography is clicked
#	Then a validation message for invalid format will appear under the mobile no. field
#
#Scenario: Apply permit for non-commercial video and photography while providing a mobile no. w/ non-numerical characters
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	When the user will fills up the form for permit for non-commercial video and photography and provide a mobile no. w/ non-numercial characters
#	Then the non-numerical character willnot be accepted in the mobile no.field
#
#Scenario: Apply permit for non-commercial video and photography while leaving the address field blank
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography leaving the address field blank
#	When the submit button for non-commercial video and photography is clicked
#	Then a required validation message will appear under the address field
#
#Scenario: Apply permit for non-commercial video and photography while leaving the email field blank
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography leaving the email field blank
#	When the submit button for non-commercial video and photography is clicked
#	Then a required validation message will appear under the email field
#	
#Scenario: Apply permit for non-commercial video and photography while providing a email w/ invalid format
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography and provide a email w/ invalid format
#	When the submit button for non-commercial video and photography is clicked
#	Then a validation message for invalid format will appear under the email field
#
#Scenario: Apply permit for non-commercial video and photography w/o selecting purpose/s
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography w/o selecting a purpose
#	When the submit button for non-commercial video and photography is clicked
#	Then the submission of the form for non-commercial video and photography will not proceed
#
#Scenario: Apply permit for non-commercial video and photography w/o selecting location/s
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography w/o selecting location/s
#	When the submit button for non-commercial video and photography is clicked
#	Then the submission of the form for non-commercial video and photography will not proceed
#
#Scenario: Apply permit for non-commercial video and photography w/o selecting detail/s
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography w/o selecting detail/s
#	When the submit button for non-commercial video and photography is clicked
#	Then the submission of the form for non-commercial video and photography will not proceed
#
#Scenario: Apply permit for non-commercial video and photography w/ valid and complete details
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography w/ valid and complete details
#	And the user will submit the form permit for non-commercial video and photography
#	And the user will go to the dashboard page to check submitted UPCDD application
#	And click the Permits an Clearances menu to check submitted UPCDD application
#	And then click the Pending Applications menu to check submitted UPCDD application
#	When the user goes to the last part of the table to look for the to check submitted UPCDD application
#	Then the submitted UPCDD application should be present
#
#Scenario: Apply permit for non-commercial video and photography w/ zero as the no. of person during the shoot for adult
#	Given That user will go to the customer portal
#	And then click the Apply BMD Services menu
#	And click Apply Application button for permit for non-commercial video and photography
#	And the user will fill up the form for permit for non-commercial video and photography and leaving the value for the adult field w/ zero as value
#	When the submit button for non-commercial video and photography is clicked
#	Then an invalid amount error message will appear under the adult field
