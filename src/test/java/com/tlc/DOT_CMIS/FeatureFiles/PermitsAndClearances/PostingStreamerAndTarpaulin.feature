@STREAMERPERMIT
Feature: Apply for permit for Posting of Streamer and Tarpaulin

@STREAMERPERMIT-001
Scenario: Apply permit for Posting of Streamer and Tarpaulin w/ incomplete details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for posting of streamer and tarpaulin
	And then fill up the form for posting of streamer and tarpaulin leaving some fields blank
	When the posting of streamer and tarpaulin form is submitted
	Then the submission of posting of streamer and tarpaulin form will not proceed
	
@STREAMERPERMIT-002	
Scenario: Apply permit for Posting of Streamer and Tarpaulin w/ blank details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for posting of streamer and tarpaulin
	And then fill up the form for posting of streamer and tarpaulin leaving all fields blank
	When the posting of streamer and tarpaulin form is submitted
	Then the submission of posting of streamer and tarpaulin form will not proceed
	
@STREAMERPERMIT-003
Scenario: Apply permit for Posting of Streamer and Tarpaulin w/ invalid email
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for posting of streamer and tarpaulin
	And then fill up the form for posting of streamer and tarpaulin w/ invalid email format
	When the submit button for posting of streamer and tarpaulin is clicked
	Then an error validation under email field will appear

@STREAMERPERMIT-004
Scenario: Apply permit for Posting of Streamer and Tarpaulin w/ a telephone no. w/ non-numerical characters
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for posting of streamer and tarpaulin
	And then fill up the form for posting of streamer and tarpaulin while using a tel. no. w/ non-numerical characters
	When the posting of streamer and tarpaulin form is submitted
	Then the submission of posting of streamer and tarpaulin form will not proceed

@STREAMERPERMIT-005	
Scenario: Apply permit for Posting of Streamer and Tarpaulin w/ a telephone no. w/ invalid lenght
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for posting of streamer and tarpaulin
	And then fill up the form for posting of streamer and tarpaulin while using a tel.no. w/ invalid length
	When the posting of streamer and tarpaulin form is submitted
	Then the submission of posting of streamer and tarpaulin form will not proceed

@STREAMERPERMIT-006
Scenario: Apply permit for Posting of Streamer and Tarpaulin w/ a mobile no. w/ non-numerical characters
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for posting of streamer and tarpaulin
	And then fill up the form for posting of streamer and tarpaulin w/ a mobile no. w/ non-numerical characters
	When the posting of streamer and tarpaulin form is submitted
	Then the submission of posting of streamer and tarpaulin form will not proceed
	
@STREAMERPERMIT-007
Scenario: Apply permit for Posting of Streamer and Tarpaulin w/ a mobile no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for posting of streamer and tarpaulin
	And then fill up the form for posting of streamer and tarpaulin w/ a mobile no. w/ invalid length
	When the posting of streamer and tarpaulin form is submitted
	Then the submission of posting of streamer and tarpaulin form will not proceed
	
@STREAMERPERMIT-008
Scenario: Apply permit for Posting of Streamer and Tarpaulin w/ valid and complete details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for posting of streamer and tarpaulin
	And then fill up the form for posting of streamer and tarpaulin w/ valid and complete details
	And the user submit the posting of streamer and tarpaulin form
	And the user will go to the dashboard page to check submitted UPCDD application
	And click the Permits an Clearances menu to check submitted UPCDD application
	And then click the Pending Applications menu to check submitted UPCDD application
	When the user goes to the last part of the table to look for the to check submitted UPCDD application
	Then the submitted UPCDD application should be present

@STREAMERPERMIT-009
Scenario: Apply permit for Posting of Streamer and Tarpaulin w/o the content/announcement
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for posting of streamer and tarpaulin
	And then fill up the form for posting of streamer and tarpaulin w/o the content/announcement
	When the posting of streamer and tarpaulin form is submitted
	Then the submission of posting of streamer and tarpaulin form will not proceed
	
@STREAMERPERMIT-010
Scenario: Apply permit for Posting of Streamer and Tarpaulin w/o the posting schedule
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for posting of streamer and tarpaulin
	And then fill up the form for posting of streamer and tarpaulin w/o the posting schedule
	When the posting of streamer and tarpaulin form is submitted
	Then the submission of posting of streamer and tarpaulin form will not proceed