@EXCAVATIONPERMIT
Feature: Apply for Excavation Permit

@EXCAVATIONPERMIT-001
Scenario: Apply permit for Excavation Clearance w/ incomplete details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for excavation clearance
	And then fill up the form for excavation clearance leaving some fields blank
	When the excavation clearance form is submitted
	Then the submission of excavation clearance form will not proceed

@EXCAVATIONPERMIT-002	
Scenario: Apply permit for Excavation Clearance w/ invalid email
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for excavation clearance
	And then fill up the form for excavation clearance w/ invalid email format
	When the submit button for excavation permit is clicked
	Then an error validation under email field will appear

@EXCAVATIONPERMIT-003
Scenario: Apply permit for Excavation Clearance w/ invalid telephone no.
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for excavation clearance
	And then fill up the form for excavation clearance w/ invalid telephone no.
	When the excavation clearance form is submitted
	Then the submission of excavation clearance form will not proceed

@EXCAVATIONPERMIT-004	
Scenario: Apply permit for Excavation Clearance w/ a mobile no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for excavation clearance
	And then fill up the form for excavation clearance w/ invalid mobile no.
	When the excavation clearance form is submitted
	Then the submission of excavation clearance form will not proceed
	
@EXCAVATIONPERMIT-005
Scenario: Apply permit for Excavation Clearance w/ valid and complete details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for excavation clearance
	And then fill up the form for excavation clearance w/ valid and complete details
	And the user submit the excavation Clearance form
	And the user will go to the dashboard page to check submitted UPCDD application
	And click the Permits an Clearances menu to check submitted UPCDD application
	And then click the Pending Applications menu to check submitted UPCDD application
	When the user goes to the last part of the table to look for the to check submitted UPCDD application
	Then the submitted UPCDD application should be present

@EXCAVATIONPERMIT-006
Scenario: Apply permit for Excavation Clearance w/ blank field values
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for excavation clearance
	And then fill up the form for excavation clearance leaving all fields blank
	When the excavation clearance form is submitted
	Then the submission of excavation clearance form will not proceed
	
@EXCAVATIONPERMIT-007	
Scenario: Apply permit for Excavation Clearance w/ a mobile no. w/ non-numerical characters
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for excavation clearance
	And then fill up the form for excavation clearance w/ mobile number w/ non-numerical characters
	When the excavation clearance form is submitted
	Then the submission of excavation clearance form will not proceed
	
@EXCAVATIONPERMIT-008
Scenario: Apply permit for Excavation Clearance w/o middle name and suffix
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for excavation clearance
	And then fill up the form for excavation clearance leaving the fields for middle name and suffix blank
	And the user submit the excavation Clearance form
	And the user will go to the dashboard page to check submitted UPCDD application
	And click the Permits an Clearances menu to check submitted UPCDD application
	And then click the Pending Applications menu to check submitted UPCDD application
	When the user goes to the last part of the table to look for the to check submitted UPCDD application
	Then the submitted UPCDD application should be present