@SIDEWALKCONSTRUCTION
Feature: Apply for Sidewalk Construction Permit

@SIDEWALKCONSTRUCTION-001
Scenario: Apply for Sidewalk Construction Permit w/ incomplete details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for sidewalk construction permit
	And the user will fill up the form for sidewalk construction permit while leaving some fields blank
	When the user submit the sidewalk construction permit form
	Then the submission of sidewalk construction permit form will not proceed
	
@SIDEWALKCONSTRUCTION-002
Scenario: Apply for Sidewalk Construction Permit w/ a telephone no. w/ non-numerical characters
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for sidewalk construction permit
	And the user will fill up the form for sidewalk construction permit w/ invalid telephone no.
	When the user submit the sidewalk construction permit form
	Then the submission of sidewalk construction permit form will not proceed

@SIDEWALKCONSTRUCTION-003
Scenario: Apply for Sidewalk Construction Permit w/ a mobile no. w/ non-numeric characters
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for sidewalk construction permit
	And the user will fill up the form for sidewalk construction permit w/ invalid mobile no.
	When the user submit the sidewalk construction permit form
	Then the submission of sidewalk construction permit form will not proceed

@SIDEWALKCONSTRUCTION-004
Scenario: Apply for Sidewalk Construction Permit w/ invalid email no.
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for sidewalk construction permit
	And the user will fill up the form for sidewalk construction permit w/ invalid email
	When the submit button for sidewalk construction permit is clicked
	Then an error validation under email field will appear

@SIDEWALKCONSTRUCTION-005
Scenario: Apply for Sidewalk Construction Permit w/ valid and complete details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for sidewalk construction permit
	And the user will fill up the form for sidewalk construction permit w/ valid and complete details
	And the user submit the sidewalk construction permit form
	And the user will go to the dashboard page to check submitted UPCDD application
	And click the Permits an Clearances menu to check submitted UPCDD application
	And then click the Pending Applications menu to check submitted UPCDD application
	When the user goes to the last part of the table to look for the to check submitted UPCDD application
	Then the submitted UPCDD application should be present
	
@SIDEWALKCONSTRUCTION-006
Scenario: Apply for Sidewalk Construction Permit w/ blank fields
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for sidewalk construction permit
	And the user will fill up the form for sidewalk construction permit while leaving all fields blank
	When the user submit the sidewalk construction permit form
	Then the submission of sidewalk construction permit form will not proceed
	
@SIDEWALKCONSTRUCTION-007
Scenario: Apply for Sidewalk Construction Permit w/ a telephone no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for sidewalk construction permit
	And the user will fill up the form for sidewalk construction permit w/ a telephone number w/ invalid length
	When the user submit the sidewalk construction permit form
	Then the submission of sidewalk construction permit form will not proceed
	
@SIDEWALKCONSTRUCTION-008
Scenario: Apply for Sidewalk Construction Permit w/ a mobile no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for sidewalk construction permit
	And the user will fill up the form for sidewalk construction permit w/ a mobile no. w/ invalid length
	When the user submit the sidewalk construction permit form
	Then the submission of sidewalk construction permit form will not proceed