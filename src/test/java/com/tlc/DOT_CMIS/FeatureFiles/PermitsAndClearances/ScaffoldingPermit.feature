@SCAFFOLDINGPERMIT
Feature: Apply for Scaffolding Permit

@SCAFFOLDINGPERMIT-001
Scenario: Apply for scaffolding permit w/ blank fields
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for scaffolding permit
	And then fill up the form for scaffolding permit leaving all fields blank
	When the submit button for scaffolding permit is clicked
	Then a required validation under scaffolding permit application fields will appear

@SCAFFOLDINGPERMIT-002	
Scenario: Apply for scaffolding permit w/o middle name and suffix
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for scaffolding permit
	And then fill up the form for scaffolding permit leaving the middle name and suffix field empty
	And the user submit the scaffolding permit form
	And the user will go to the dashboard page to check submitted UPCDD application
	And click the Permits an Clearances menu to check submitted UPCDD application
	And then click the Pending Applications menu to check submitted UPCDD application
	When the user goes to the last part of the table to look for the to check submitted UPCDD application
	Then the submitted UPCDD application should be present

@SCAFFOLDINGPERMIT-003	
Scenario: Apply for scaffolding permit w/ telephone no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for scaffolding permit
	When the user fills up the form for scaffolding permit and provided a telephone no.w/ invalid length
	And the user clicked the submit button for scaffolding permit
	Then an Invalid Format error message will appear under the telephone no.field

@SCAFFOLDINGPERMIT-004
Scenario: Apply for scaffolding permit w/ mobile no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for scaffolding permit
	When the user fills up the form for scaffolding permit and provided a mobile no.w/ invalid length
	And the user clicked the submit button for scaffolding permit
	Then an Invalid Format error message will appear under the mobile no.field
	
@SCAFFOLDINGPERMIT-005
Scenario: Apply for scaffolding permit w/ telephone no. w/ non-numerical characters
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for scaffolding permit
	When the user fills up the form for scaffolding permit and provided a telephone no.w/ non-numerical characters 
	Then the non-numerical characters will not be accepted by the telephone no. field


@SCAFFOLDINGPERMIT-006
Scenario: Apply for scaffolding permit w/ mobile no.w/ non-numerical characters
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for scaffolding permit
	When the user fills up the form for scaffolding permit and provided a mobile no.w/ non-numerical characters 
	Then the non-numerical characters will not be accepted by the mobile no. field

@SCAFFOLDINGPERMIT-007
Scenario: Apply for scaffolding permit w/o email address
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for scaffolding permit
	And the user will fill up the form for scaffolding permit leaving the email field blank
	When the submit button for scaffolding permit is clicked
	Then A required validation under the email field will appear

@SCAFFOLDINGPERMIT-008	
Scenario: Apply for scaffolding permit w/ invalid email address format
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for scaffolding permit
	When the user fills up the form for scaffolding permit w/ an email with invalid format
	And the user clicked the submit button for scaffolding permit
	Then A Please enter a valid email address message will appear under the email field

@SCAFFOLDINGPERMIT-009
Scenario: Apply for scaffolding permit w/o the required details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for scaffolding permit
	And the user will fill up the form for scaffolding permit w/ only the middle name and suffix and leaving the other fields blank
	When the submit button for scaffolding permit is clicked
	Then a required validation under scaffolding permit application fields will appear

@SCAFFOLDINGPERMIT-010
Scenario: Apply for scaffolding permit w/ valid and complete details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for scaffolding permit
	And then fill up the form for scaffolding permit w/ valid and complete details
	And the user submit the scaffolding permit form
	And the user will go to the dashboard page to check submitted UPCDD application
	And click the Permits an Clearances menu to check submitted UPCDD application
	And then click the Pending Applications menu to check submitted UPCDD application
	When the user goes to the last part of the table to look for the to check submitted UPCDD application
	Then the submitted UPCDD application should be present

