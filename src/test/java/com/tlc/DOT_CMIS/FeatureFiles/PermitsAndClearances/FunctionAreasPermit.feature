@FUNCTIONAREASPERMIT
Feature: Apply for Permit for Use of Function Areas

@FUNCTIONAREASPERMIT-001
Scenario: Apply permit for use of function areas w/ incomplete details
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit leaving some fields blank
	When the form for the Function Areas Permit is submitted
	Then the submission of application for Function Areas Permit will not proceed
	
@FUNCTIONAREASPERMIT-002	
Scenario: Apply permit for use of function areas w/ a telephone no. w/ non-numercial characters
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit w/ invalid telephone no. format
	When the form for the Function Areas Permit is submitted
	Then the submission of application for Function Areas Permit will not proceed

@FUNCTIONAREASPERMIT-003
Scenario: Apply permit for use of function areas w/ a mobile no. w/ non-numerical characters
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit w/ invalid mobile no. format
	When the form for the Function Areas Permit is submitted
	Then the submission of application for Function Areas Permit will not proceed

@FUNCTIONAREASPERMIT-004	
Scenario: Apply permit for use of function areas w/ invalid email format
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit w/ an invalid email format
	When the submit button for Function Areas Permit is clicked
	Then an error validation under email field will appear

@FUNCTIONAREASPERMIT-005
Scenario: Apply permit for use of function areas w/o selecting a purpose
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit w/o selecting a purpose
	When the form for the Function Areas Permit is submitted
	Then the submission of application for Function Areas Permit will not proceed

@FUNCTIONAREASPERMIT-006
Scenario: Apply permit for use of function areas w/o the names of the event owner
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit w/o providing the name/s of the event owner/s
	When the form for the Function Areas Permit is submitted
	Then the submission of application for Function Areas Permit will not proceed

@FUNCTIONAREASPERMIT-007
Scenario: Apply permit for use of function areas w/o selecting a venue
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit w/o selecting a venue
	When the form for the Function Areas Permit is submitted
	Then the submission of application for Function Areas Permit will not proceed
	
@FUNCTIONAREASPERMIT-008
Scenario: Apply permit for use of function areas w/ a text input for the no. of guest field
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit w/ a text input for the no. of guest field
	When the form for the Function Areas Permit is submitted
	Then the submission of application for Function Areas Permit will not proceed

@FUNCTIONAREASPERMIT-009
Scenario: Apply permit for use of function areas w/ a text input for extension field
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit w/ a text input for the extension field
	When the form for the Function Areas Permit is submitted
	Then the submission of application for Function Areas Permit will not proceed
	
@FUNCTIONAREASPERMIT-010
Scenario: Apply permit for use of function areas w/ valid and complete details
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit w/ valid and complete details
	And the form for the Function Areas Permit will be submitted
	And the user will go to the dashboard page
	And click the Permits an Clearances menu
	And then click the Pending Applications menu
	When the user goes to the last part of the table of for permits
	Then the submitted application for function areas permit should be present
	
@FUNCTIONAREASPERMIT-011
Scenario: Apply permit for use of function areas w/ blank fields
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit leaving all fields blank
	When the form for the Function Areas Permit is submitted
	Then the submission of application for Function Areas Permit will not proceed
	
@FUNCTIONAREASPERMIT-012
Scenario: Apply permit for use of function areas w/ a mobile no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit w/ a mobile number w/ invalid length
	When the form for the Function Areas Permit is submitted
	Then the submission of application for Function Areas Permit will not proceed
	
@FUNCTIONAREASPERMIT-013	
Scenario: Apply permit for use of function areas w/ a telephone no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit w/ a telephone no. w/ invalid length
	When the form for the Function Areas Permit is submitted
	Then the submission of application for Function Areas Permit will not proceed
	
@FUNCTIONAREASPERMIT-014
Scenario: Apply permit for use of function areas leaving the middle name and suffix fields blank
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Function Areas Permit
	And then fill up the form for Function Areas Permit leaving the middle name and suffix fields blank
	And the form for the Function Areas Permit will be submitted
	And the user will go to the dashboard page
	And click the Permits an Clearances menu
	And then click the Pending Applications menu
	When the user goes to the last part of the table of for permits
	Then the submitted application for function areas permit should be present