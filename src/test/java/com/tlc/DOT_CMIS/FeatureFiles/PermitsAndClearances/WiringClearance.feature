@WIRINGCLEARANCE
Feature: Apply for Wiring Clearance

@UPCDDSRVC-WIRINGPERMIT-001
Scenario: Apply for wiring clearance w/ incomplete details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for wiring clearance
	And the user will fill up the form for wiring clearance while leaving some fields blank
	When the user submit the wiring clearance form
	Then the submission of wiring clearance form will not proceed

@UPCDDSRVC-WIRINGPERMIT-002	
Scenario: Apply for wiring clearance w/ a telephone no. w/ non-numeric characters
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for wiring clearance
	And the user will fill up the form for wiring clearance w/ invalid telephone number
	When the user submit the wiring clearance form
	Then the submission of wiring clearance form will not proceed
	
@UPCDDSRVC-WIRINGPERMIT-003
Scenario: Apply for wiring clearance w/ a mobile no. w/ non-numeric characters
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for wiring clearance
	And the user will fill up the form for wiring clearance w/ invalid mobile number
	When the user submit the wiring clearance form
	Then the submission of wiring clearance form will not proceed

@UPCDDSRVC-WIRINGPERMIT-004
Scenario: Apply for wiring clearance w/ invalid email
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for wiring clearance
	And the user will fill up the form for wiring clearance w/ invalid email
	When the submit button for wiring clearance is clicked
	Then an error validation under email field will appear

@UPCDDSRVC-WIRINGPERMIT-005
Scenario: Apply for wiring clearance w/ non-numerical floor/lot area value
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for wiring clearance
	And the user will fill up the form for wiring clearance w/ non-numerical value for floor/lot area field
	When the user submit the wiring clearance form
	Then the submission of wiring clearance form will not proceed

@UPCDDSRVC-WIRINGPERMIT-006
Scenario: Apply for wiring clearance w/ no selected purpose
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for wiring clearance
	And the user will fill up the form for wiring clearance w/ no selected purpose
	When the user submit the wiring clearance form
	Then the submission of wiring clearance form will not proceed

@UPCDDSRVC-WIRINGPERMIT-007
Scenario: Apply for wiring clearance w/ valid and complete details
Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for wiring clearance
	And the user will fill up the form for wiring clearance w/ valid and complete details
	And the user submit the wiring clearance form
	And the user will go to the dashboard page to check submitted UPCDD application
	And click the Permits an Clearances menu to check submitted UPCDD application
	And then click the Pending Applications menu to check submitted UPCDD application
	When the user goes to the last part of the table to look for the to check submitted UPCDD application
	Then the submitted UPCDD application should be present
	
@UPCDDSRVC-WIRINGPERMIT-008
Scenario: Apply for wiring clearance w/ blank fields
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for wiring clearance
	And the user will fill up the form for wiring clearance while leaving all fields blank
	When the user submit the wiring clearance form
	Then the submission of wiring clearance form will not proceed
	
@UPCDDSRVC-WIRINGPERMIT-009	
Scenario: Apply for wiring clearance w/ a telephone no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for wiring clearance
	And the user will fill up the form for wiring clearance w/ a telephone number w/ invalid length
	When the user submit the wiring clearance form
	Then the submission of wiring clearance form will not proceed
	
@UPCDDSRVC-WIRINGPERMIT-010
Scenario: Apply for wiring clearance w/ a mobile no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for wiring clearance
	And the user will fill up the form for wiring clearance w/ a mobile no. w/ invalid length
	When the user submit the wiring clearance form
	Then the submission of wiring clearance form will not proceed