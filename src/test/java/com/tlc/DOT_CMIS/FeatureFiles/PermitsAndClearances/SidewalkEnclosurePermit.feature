@SIDEWALKENCLOSURE
Feature: Apply for Temporary Sidewalk Enclosure and Occupancy Permit

@SIDEWALKENCLOSURE-001
Scenario: Apply for Temporary Sidewalk/Street Enclosure and Occupancy permit w/ incomplete details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for temporary sidewalk/street enclosure and occupancy permit
	And the user will fill up the form for enclosure permit while leaving some fields blank
	When user submitted the form for enclosure permit
	Then the submission of enclosure permit form will not proceed

@SIDEWALKENCLOSURE-002
Scenario: Apply for Temporary Sidewalk/Street Enclosure and Occupancy permit w/ a telephone no. w/ non-numeric characters
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for temporary sidewalk/street enclosure and occupancy permit
	And the user will fill up the form for enclosure permit w/ invalid telephone number
	When user submitted the form for enclosure permit
	Then the submission of enclosure permit form will not proceed

@SIDEWALKENCLOSURE-003
Scenario: Apply for Temporary Sidewalk/Street Enclosure and Occupancy permit w/ a mobile no. w/ non-numeric characters
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for temporary sidewalk/street enclosure and occupancy permit
	And the user will fill up the form for enclosure permit w/ invalid mobile number
	When user submitted the form for enclosure permit
	Then the submission of enclosure permit form will not proceed

@SIDEWALKENCLOSURE-004
Scenario: Apply for Temporary Sidewalk/Street Enclosure and Occupancy permit w/ invalid email
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for temporary sidewalk/street enclosure and occupancy permit
	And the user will fill up the form for enclosure permit w/ invalid email
	When the submit button for enclosure permit is clicked
	Then an error validation under email field will appear

@SIDEWALKENCLOSURE-005
Scenario: Apply for Temporary Sidewalk/Street Enclosure and Occupancy permit w/ valid and complete details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for temporary sidewalk/street enclosure and occupancy permit
	And the user will fill up the form for enclosure permit w/ valid and complete details
	And the user submit the enclosure permit form
	And the user will go to the dashboard page to check submitted UPCDD application
	And click the Permits an Clearances menu to check submitted UPCDD application
	And then click the Pending Applications menu to check submitted UPCDD application
	When the user goes to the last part of the table to look for the to check submitted UPCDD application
	Then the submitted UPCDD application should be present
	
@SIDEWALKENCLOSURE-006
Scenario: Apply for Temporary Sidewalk/Street Enclosure and Occupancy permit w/ blank fields
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for temporary sidewalk/street enclosure and occupancy permit
	And the user will fill up the form for enclosure permit while leaving all fields blank
	When user submitted the form for enclosure permit
	Then the submission of enclosure permit form will not proceed
	
@SIDEWALKENCLOSURE-007
Scenario: Apply for Temporary Sidewalk/Street Enclosure and Occupancy permit w/ a mobile no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for temporary sidewalk/street enclosure and occupancy permit
	And the user will fill up the form for enclosure permit w/ a mobile number w/ invalid length
	When user submitted the form for enclosure permit
	Then the submission of enclosure permit form will not proceed
	
@SIDEWALKENCLOSURE-008
Scenario: Apply for Temporary Sidewalk/Street Enclosure and Occupancy permit w/ a telephone no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for temporary sidewalk/street enclosure and occupancy permit
	And the user will fill up the form for enclosure permit w/ a telephone no. w/ invalid length
	When user submitted the form for enclosure permit
	Then the submission of enclosure permit form will not proceed