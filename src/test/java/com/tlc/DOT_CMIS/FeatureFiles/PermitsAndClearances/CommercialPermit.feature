@COMMERCIALPERMIT
Feature: Apply for Permit for Commercial Photography, Videos and Documentaries and Shooting Movies

@COMMERCIALPERMIT-001	
Scenario: Apply permit for commercial photography, videos and documentaries and shooting movies w/ incomplete details
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Commercial Shooting Permit
	And then fill up the form for Commercial Shooting Permit leaving some fields blank
	When the form for the Commercial Shooting Permit is submitted
	Then the submission of application for commercial photography, videos and documentaries and shooting movies will not proceed

@COMMERCIALPERMIT-002	
Scenario: Apply permit for commercial photography, videos and documentaries and shooting movies using an invalid email format
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Commercial Shooting Permit
	And then fill up the form for Commercial Shooting Permit with an email w/ invalid format
	When the form for the Commercial Shooting Permit is submitted
	Then the submission of application for commercial photography, videos and documentaries and shooting movies will not proceed

@COMMERCIALPERMIT-003
Scenario: Apply permit for commercial photography, videos and documentaries and shooting movies using a telephone no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Commercial Shooting Permit
	And then fill up the form for Commercial Shooting Permit with a telephone no. w/ invalid format
	When the form for the Commercial Shooting Permit is submitted
	Then the submission of application for commercial photography, videos and documentaries and shooting movies will not proceed
	
@COMMERCIALPERMIT-004
Scenario: Apply permit for commercial photography, videos and documentaries and shooting movies using a mobile no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Commercial Shooting Permit
	And then fill up the form for Commercial Shooting Permit with a mobile no. w/ invalid format
	When the form for the Commercial Shooting Permit is submitted
	Then the submission of application for commercial photography, videos and documentaries and shooting movies will not proceed
	
@COMMERCIALPERMIT-005
Scenario: Apply permit for commercial photography, videos and documentaries and shooting movies w/ valid and complete details
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Commercial Shooting Permit
	And then fill up the form for Commercial Shooting Permit w/ valid and complete details
	And the form for the Commercial Shooting Permit will be submitted
	And the user will go to the dashboard page
	And click the Permits an Clearances menu
	And then click the Pending Applications menu
	When the user goes to the last part of the table of for permits
	Then the submitted application for commercial photography, videos and documentaries and shooting movies should be present
	
@COMMERCIALPERMIT-006
Scenario: Apply permit for commercial photography, videos and documentaries and shooting movies w/ blank fields
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Commercial Shooting Permit
	And then fill up the form for Commercial Shooting Permit with a mobile no. w/ blank fields
	When the form for the Commercial Shooting Permit is submitted
	Then the submission of application for commercial photography, videos and documentaries and shooting movies will not proceed
	
@COMMERCIALPERMIT-007
Scenario: Apply permit for commercial photography, videos and documentaries and shooting movies using a telephone no. w/ non-numerical characters
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Commercial Shooting Permit
	And then fill up the form for Commercial Shooting Permit with a telephone no. w/ non-numerical characters
	When the form for the Commercial Shooting Permit is submitted
	Then the submission of application for commercial photography, videos and documentaries and shooting movies will not proceed
	
@COMMERCIALPERMIT-008
Scenario: Apply permit for commercial photography, videos and documentaries and shooting movies w/o middle name and suffix
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Commercial Shooting Permit
	And then fill up the form for Commercial Shooting Permit w/o middle name and suffix
	And the form for the Commercial Shooting Permit will be submitted
	And the user will go to the dashboard page
	And click the Permits an Clearances menu
	And then click the Pending Applications menu
	When the user goes to the last part of the table of for permits
	Then the submitted application for commercial photography, videos and documentaries and shooting movies should be present
	
@COMMERCIALPERMIT-009
Scenario: Apply permit for commercial photography, videos and documentaries and shooting movies using a mobile no. w/ non-numerical characters
	Given That user will go to the customer portal
	And then click the Apply BMD Services menu
	And then click Apply Application button for Commercial Shooting Permit
	And then fill up the form for Commercial Shooting Permit with a mobile no. w/ non-numerical characters
	When the form for the Commercial Shooting Permit is submitted
	Then the submission of application for commercial photography, videos and documentaries and shooting movies will not proceed