@LOCATIONALPERMIT
Feature: Apply for Locational Permit

@LOCATIONALPERMIT-001	
Scenario: Apply permit for Locational Clearance w/ incomplete details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for locational clearance
	And then fill up the form for locational clearance leaving some fields blank
	When the locational clearance form is submitted
	Then the submission of locational clearance form will not proceed

@LOCATIONALPERMIT-002	
Scenario: Apply permit for Locational Clearance w/ invalid email
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for locational clearance
	And then fill up the form for locational clearance w/ invalid email format
	When the submit button for locational permit is clicked
	Then an error validation under email field will appear

@LOCATIONALPERMIT-003
Scenario: Apply permit for Locational Clearance w/ a telephone no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for locational clearance
	And then fill up the form for locational clearance w/ invalid telephone no.
	When the locational clearance form is submitted
	Then the submission of locational clearance form will not proceed

@LOCATIONALPERMIT-004	
Scenario: Apply permit for Locational Clearance w/ a mobile no. w/ invalid length
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for locational clearance
	And then fill up the form for locational clearance w/ invalid mobile no.
	When the locational clearance form is submitted
	Then the submission of locational clearance form will not proceed
	
@LOCATIONALPERMIT-005
Scenario: Apply permit for Locational Clearance w/ valid and complete details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for locational clearance
	And then fill up the form for locational clearance w/ valid and complete details
	And the user submit the Locational Clearance form
	And the user will go to the dashboard page to check submitted UPCDD application
	And click the Permits an Clearances menu to check submitted UPCDD application
	And then click the Pending Applications menu to check submitted UPCDD application
	When the user goes to the last part of the table to look for the to check submitted UPCDD application
	Then the submitted UPCDD application should be present
	
@LOCATIONALPERMIT-006	
Scenario: Apply permit for Locational Clearance w/ blank details
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for locational clearance
	And then fill up the form for locational clearance leaving all fields blank
	When the locational clearance form is submitted
	Then the submission of locational clearance form will not proceed
	
@LOCATIONALPERMIT-007	
Scenario: Apply permit for Locational Clearance w/ a mobile no. w/ non-numerical characters
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for locational clearance
	And then fill up the form for locational clearance w/ a mobile no. w/ non-numerical characters
	When the locational clearance form is submitted
	Then the submission of locational clearance form will not proceed
	
@LOCATIONALPERMIT-008
Scenario: Apply permit for Locational Clearance w/ a telephone no. w/ non-numerical values
	Given That user will go to the customer portal
	And then click the Apply UPCDD Services menu
	And click Apply Application button for locational clearance
	And then fill up the form for locational clearance w/ a telephone no. w/ non-numerical values
	When the locational clearance form is submitted
	Then the submission of locational clearance form will not proceed